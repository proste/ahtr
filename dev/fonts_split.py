import os
from glob import glob

import numpy as np

from hawran import DATA_DIR

# TODO make stable and appendable

rng = np.random.default_rng(42)
l = [p.split('/')[-1].split('.')[-2] for p in glob(f'{DATA_DIR}/fonts/*.ttf')]
rng.shuffle(l)
*train, val, test = np.array_split(l, 4)
train = np.concatenate(train).tolist()
val = val.tolist()
test = test.tolist()

for names, split in [(train, 'train'), (val, 'val'), (test, 'test')]:
    os.makedirs(f'{DATA_DIR}/fonts/{split}', exist_ok=True)
    for name in names:
        os.rename(f'{DATA_DIR}/fonts/{name}.ttf', f'{DATA_DIR}/fonts/{split}/{name}.ttf')
        os.rename(f'{DATA_DIR}/fonts/{name}.txt', f'{DATA_DIR}/fonts/{split}/{name}.txt')
