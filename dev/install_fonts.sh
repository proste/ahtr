# Downloads selection of .ttf fonts (and their licences) to current directory

# GoogleFonts
URL_PREFIX="https://github.com/google/fonts/raw/main"
FONTS="\
ofl/aguafinascript AguafinaScript-Regular.ttf OFL.txt
ofl/alexbrush AlexBrush-Regular.ttf OFL.txt
ofl/allura Allura-Regular.ttf OFL.txt
ofl/arizonia Arizonia-Regular.ttf OFL.txt
ofl/bilbo Bilbo-Regular.ttf OFL.txt
apache/calligraffitti Calligraffitti-Regular.ttf LICENSE.txt
ofl/cedarvillecursive Cedarville-Cursive.ttf OFL.txt
ofl/clickerscript ClickerScript-Regular.ttf OFL.txt
ofl/cookie Cookie-Regular.ttf OFL.txt
ofl/damion Damion-Regular.ttf OFL.txt
ofl/dawningofanewday DawningofaNewDay.ttf OFL.txt
ofl/euphoriascript EuphoriaScript-Regular.ttf OFL.txt
ofl/greatvibes GreatVibes-Regular.ttf OFL.txt
ofl/herrvonmuellerhoff HerrVonMuellerhoff-Regular.ttf OFL.txt
apache/homemadeapple HomemadeApple-Regular.ttf LICENSE.txt
ofl/italianno Italianno-Regular.ttf OFL.txt
ofl/kristi Kristi-Regular.ttf OFL.txt
ofl/labelleaurore LaBelleAurore.ttf OFL.txt
ofl/leaguescript LeagueScript-Regular.ttf OFL.txt
ofl/leckerlione LeckerliOne-Regular.ttf OFL.txt
ofl/marckscript MarckScript-Regular.ttf OFL.txt
ofl/meddon Meddon.ttf OFL.txt
ofl/monsieurladoulaise MonsieurLaDoulaise-Regular.ttf OFL.txt
apache/montez Montez-Regular.ttf LICENSE.txt
ofl/mrdafoe MrDafoe-Regular.ttf OFL.txt
ofl/mrdehaviland MrDeHaviland-Regular.ttf OFL.txt
ofl/mrssaintdelafield MrsSaintDelafield-Regular.ttf OFL.txt
ofl/niconne Niconne-Regular.ttf OFL.txt
ofl/norican Norican-Regular.ttf OFL.txt
ofl/pacifico Pacifico-Regular.ttf OFL.txt
ofl/parisienne Parisienne-Regular.ttf OFL.txt
ofl/petitformalscript PetitFormalScript-Regular.ttf OFL.txt
ofl/pinyonscript PinyonScript-Regular.ttf OFL.txt
ofl/qwigley Qwigley-Regular.ttf OFL.txt
ofl/reeniebeanie ReenieBeanie.ttf OFL.txt
apache/rochester Rochester-Regular.ttf LICENSE.txt
ofl/rougescript RougeScript-Regular.ttf OFL.txt
ofl/sacramento Sacramento-Regular.ttf OFL.txt
apache/satisfy Satisfy-Regular.ttf LICENSE.txt
ofl/tangerine Tangerine-Regular.ttf OFL.txt
ofl/vibur Vibur-Regular.ttf OFL.txt
apache/yellowtail Yellowtail-Regular.ttf LICENSE.txt
ofl/yesteryear Yesteryear-Regular.ttf OFL.txt
ofl/zeyada Zeyada.ttf OFL.txt"

echo "Installing Google Fonts"
while read prefix font license
do
	name=$(echo $prefix | cut -d/ -f2)
	{ \
		curl --fail -sL "$URL_PREFIX/$prefix/$font" > "${name}.ttf" \
		&& curl --fail -sL "$URL_PREFIX/$prefix/$license" > "${name}.txt" \
		&& echo -n .;
	} || { \
		rm -r $dest && echo Installation of $font failed.; \
	}
done <<< $FONTS
echo

echo "[SUCCESS]"
