#!/usr/bin/env bash

apt-get update

# jupyterlab widgets bin deps
curl -sL https://deb.nodesource.com/setup_14.x | bash -
apt-get install -y nodejs

# interactive
pip install --no-cache-dir \
    ipdb \
    ipywidgets \
    ipympl \
    ipython \
    jupyterlab \
    tensorboard-plugin-profile \

# development
pip install --no-cache-dir \
    pytest

# jupyter setup
jupyter labextension install \
    @jupyter-widgets/jupyterlab-manager \
    jupyter-matplotlib

apt-get clean
