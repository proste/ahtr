#!/usr/bin/env bash

set -e

TMUX_SESSION=hawran


sub_dispatch(){
    ssh_host=$1
    remote_basedir=$2
    init_cmd=$3

    set -x

    current_branch=$(git branch --show-current)

    git checkout --detach HEAD
    vim entrypoint.sh
    git add entrypoint.sh

    # TODO add checks for valid name aaa-bbb-ccc
    read -i $(git describe --always) -p "Enter experiment name: " -e experiment_name

    description=":alembic: Experiment ${experiment_name}"

    git commit --allow-empty -m "${description}" || :
    experiment_name="${experiment_name}-$(git rev-parse --short HEAD)"
    tag=experiment-${experiment_name}
    git tag -a -e -m "${description}" ${tag}
    git push origin ${tag}

    remote_pwd=${remote_basedir}/${experiment_name}

    git archive --format=tar HEAD | ssh ${ssh_host} \
    "mkdir \"${remote_pwd}\" && cd \"${remote_pwd}\" && tar -xf -"

    git checkout "${current_branch:-master}"

    ssh -t ${ssh_host} "cd \"${remote_pwd}\" && ./dev/run_experiment.sh ${init_cmd}"

    set +x
}

sub_init_aic(){
    experiment_name=$(basename ${PWD})
    qsub \
      -q gpu.q \
      -cwd \
      -b y \
      -pe smp 2 \
      -l gpu=1,mem_free=16G,act_mem_free=16G,h_data=16G \
      -N ${experiment_name} \
      -j y \
      -o console.log \
      ./dev/run_experiment.sh aic_run

    if ! tmux has-session -t ${TMUX_SESSION} 2>/dev/null; then
        tmux new-session -d -c "${PWD}/../.." -s ${TMUX_SESSION}
    fi
    tmux new-window -c ${PWD} -n ${experiment_name} -t ${TMUX_SESSION} \
        "tail -F console.log 2>/dev/null"
    tmux attach-session -t ${TMUX_SESSION}
}

sub_aic_run(){
    set -x

    # setup MinIO tunnel
    while true; do
        port=$(shuf -i 42000-42999 -n 1)
	nc -z localhost $port || break
    done

    ssh_socket=$(mktemp -d /tmp/prochas7.hawran.XXXXXX)/ssh.sock
    ssh -fMN -S ${ssh_socket} -L ${port}:localhost:42009 ul5_fwd

    {
        # setup python virtual environment
        python3 -m venv dev/env-lab
        ln -s dev/env-lab .venv
        . .venv/bin/activate
        pip install --upgrade pip setuptools wheel
        ./dev/install_py_deps.sh
        pip install ./pkg/bs_decoder
        pip install -e ./pkg/ahmp ./pkg/hawran

        # set environment variables
        set -a
        EXPERIMENT_NAME=$(basename ${PWD})
        . ../.env
        MC_HOST_ul5=${MC_HOST_ul5}:${port}
        S3_ENDPOINT=${S3_ENDPOINT}:${port}
        set +a

        # run experiment
        ./entrypoint.sh
    } || :

    # teardown
    ssh -S ${ssh_socket} -O exit ul5_fwd
    rm -rf dev/env-lab

    set +x
}

sub_init_docker(){
    session_name=hawran
    if ! tmux has-session -t ${TMUX_SESSION} 2>/dev/null; then
        tmux new-session -d -c "${PWD}/../.." -s ${TMUX_SESSION} -n "monitor" "top"
        tmux split-window -h "nvidia-smi \
    --query-gpu=timestamp,pstate,power.draw,temperature.gpu,clocks.sm,utilization.gpu,memory.used,utilization.memory \
    --format=csv -l"
    fi

    tmux new-window -c ${PWD} -n $(basename ${PWD}) "./dev/run_experiment.sh docker_run; tail -f /dev/null" \; \
        pipe-pane -O "cat > ${PWD}/console.log"
    tmux attach-session -t ${TMUX_SESSION}
}

sub_docker_run(){
    id_dir=$(mktemp -d /tmp/hawran.id.XXXXXX)
    iid_f=${id_dir}/iid
    cid_f=${id_dir}/cid

    set -x

    sudo docker build \
        --iidfile ${iid_f} \
        --target lab \
        --build-arg GPU_RUNTIME="nvidia" \
        --file ./dev/Dockerfile \
        .

    iid=$(cat ${iid_f})

    date +'>>>> Started at %D-%T <<<<'

    sudo docker run \
        --cidfile ${cid_f} \
        --runtime nvidia \
        --env EXPERIMENT_NAME=$(basename ${PWD}) \
	--env CUDA_VISIBLE_DEVICES=0 \
        --env-file "${PWD}/../.env" \
        ${iid} || :

    set +x

    cid=$(cat ${cid_f})

    date +'>>>> Finished at %D-%T <<<<'
    echo "- container_id: ${cid}"
}

subcommand=$1
case $subcommand in
    "" | "-h" | "--help")
        echo TBA
	;;
    *)
        shift
        sub_${subcommand} $@
        if [ $? = 127 ]; then
            echo "Error: '$subcommand' is not a known subcommand." >&2
            echo "       Run '$ProgName --help' for a list of known subcommands." >&2
            exit 1
        fi
        ;;
esac
