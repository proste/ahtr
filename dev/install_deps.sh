#!/usr/bin/env sh

apt-get update

# convenience tools
apt-get install -y \
    less \
    vim \
    tmux \

# pillow-simd bin deps
apt-get install -y \
    libjpeg-turbo8-dev \
    zlib1g-dev \
    libtiff5-dev \
    liblcms2-dev \
    libfreetype6-dev \
    libwebp-dev \
    libharfbuzz-dev \
    libfribidi-dev \
    libopenjp2-7-dev \
    libraqm0 \

# opencv bin deps
apt-get install -y \
    ffmpeg \
    libsm6 \
    libxext6 \
    libxrender-dev \

# build tools (pip, kenlm)
apt-get install -y \
    python3-venv \
    build-essential \
    cmake \
    git \
    libboost-system-dev \
    libboost-thread-dev \
    libboost-program-options-dev \
    libboost-test-dev \
    libeigen3-dev \
    libbz2-dev \
    liblzma-dev \
    zlib1g-dev \

mkdir -p /root/bin

# minio CLI
curl -sL https://dl.min.io/client/mc/release/linux-amd64/mc > /root/bin/mc
chmod +x /root/bin/mc
SHELL=/bin/bash mc --autocompletion

# kenlm
pwd_ckpt=${PWD}

git clone --depth 1 https://github.com/kpu/kenlm.git /root/kenlm
mkdir -p /root/kenlm/build
cd /root/kenlm/build

cmake -DKENLM_MAX_ORDER=8 ..
make -j 4

cat - > /root/bin/kenlm <<'EOF'
#!/usr/bin/env sh

subcommand=$1
shift
exec /root/kenlm/build/bin/${subcommand} $@
EOF
chmod +x /root/bin/kenlm

cd ${pwd_ckpt}

apt-get clean
