#!/usr/bin/env sh

# activate python environment
. .venv/bin/activate

# run dev tooling
tmux new-session -d \
  ". .venv/bin/activate && \
  jupyter lab --ip='0.0.0.0' --port=8888 --no-browser --allow-root --notebook-dir='playground'"
tmux split-window -v \
  "tensorboard \
    --logdir=s3://hawran/tensorboard/ \
    --host=0.0.0.0 \
    --port=6006 \
    --reload_interval=300 \
    --window_title='[DEV] TensorBoard'"

# wait forever
tail -f /dev/null
