import json
import logging
import os
import random
import urllib
from time import sleep

import numpy as np
import requests
from lxml import etree, html
from PIL import Image
from tqdm import tqdm


logger = logging.getLogger(__name__)

CACHE_DIR = os.path.join(os.getcwd(), 'ahmp_data')


# TODO(proste) rewrite to init with xid auto derivation and optional fetch
class Book:
    FILENAME = 'book.json'
    PERMALINK_TEMPLATE = 'http://katalog.ahmp.cz/pragapublica/permalink?xid={xid}'

    def __init__(self, xid, name, img_repo, img_names, cache_dir):
        self._xid = xid
        self._name = name
        self._img_repo = img_repo
        self._img_names = tuple(img_names)
        self._cache_dir = cache_dir

    @classmethod
    def fetch(cls, permalink, cache_dir=CACHE_DIR):
        PAGINATION = 10

        logger.info(f'Scraping metadata for {permalink})')

        # get document-level properties
        xid = permalink.split('?')[1].split('=')[1]
        front_view = cls._get_view(permalink, 0)

        name = front_view.xpath('//div[@id="headerTopPanel"]//a/text()')[0]
        page_count = int(front_view.xpath('//div[@class="pageNum"]/text()')[0].split('|')[1].strip())
        img_repo = cls._get_scan_path(front_view, 0).rsplit('/', 2)[0].replace('/image/', '/zoomify/', 1)

        # get page names
        img_names = []
        for view_i in range(0, page_count, PAGINATION):
            view = front_view if (view_i == 0) else cls._get_view(permalink, view_i)
            img_names.extend(
                cls._get_scan_path(view, s_i).rsplit('/', 2)[1].split('.')[0]
                for s_i in range(view_i, min(page_count, view_i+PAGINATION))
            )

        book = cls(xid, name, img_repo, img_names, cache_dir)
        book._save()

        return book

    @classmethod
    def load(cls, xid, cache_dir=CACHE_DIR):
        # TODO(proste) allow FS path/URL/xid all as parameters, call fetch internally if needed

        with open(os.path.join(cache_dir, xid, cls.FILENAME), 'r') as book_f:
            dct = json.load(book_f)
        return cls(**dct, cache_dir=cache_dir)

    @property
    def xid(self):
        return self._xid

    @property
    def name(self):
        return self._name

    @property
    def permalink(self):
        return self.PERMALINK_TEMPLATE.format(xid=self._xid)

    @property
    def _dirname(self):
        return os.path.join(self._cache_dir, self._xid)

    def __len__(self):
        return len(self._img_names)

    def page(self, page_i, zoom=0):
        uuid = self._img_names[page_i]

        # try loading from cache
        page_path = os.path.join(self._dirname, f'{zoom}-{page_i:03d}-{uuid}.jpg')
        if os.path.isfile(page_path):
            page = Image.open(page_path)
            page.load()
            logger.debug(f'Loading cached page {page_i} (zoom {zoom})')
            return page, page_path

        logger.info(f'Fetching render {page_i} (zoom {zoom})')

        # touch page view so that tiles are available
        self._get_view(self.permalink, page_i)

        img_repo = f'{self._img_repo}/{uuid}.jpg'

        properties = dict(etree.parse(f'{img_repo}/ImageProperties.xml').getroot().items())
        img_size = np.array([int(properties['HEIGHT']), int(properties['WIDTH'])], dtype=np.int32)
        tile_index = int(properties['NUMTILES'])
        tile_size = int(properties['TILESIZE'])

        tile_shapes = []
        while True:
            th, tw = (img_size + (tile_size - 1)) // tile_size
            tile_index -= th * tw
            tile_shapes.append((img_size[0], img_size[1], th, tw, tile_index))
            
            if (img_size <= tile_size).all():
                break
            
            img_size = (img_size + 1) // 2
            

        tile_shapes = tile_shapes[::-1]

        assert tile_shapes[0][-1] == 0

        h, w, th, tw, tile_i = tile_shapes[zoom]
        tiles = np.empty((th, tw), dtype=object)
        for row_i in range(th):
            for col_i in range(tw):
                url = f'{self._img_repo}/{uuid}.jpg/TileGroup{tile_i // 256}/{zoom}-{col_i}-{row_i}.jpg'
                with urllib.request.urlopen(url) as tile_f:
                    tile = Image.open(tile_f)
                    tile.load()
                logger.debug(f'Fetching tile {page_i}[{zoom},{col_i},{row_i}] ({"x".join(map(str, tile.size))}px)')
                tiles[row_i, col_i] = tile
                tile_i += 1

        # collate images
        result = Image.new('RGB', (w, h))
        for row_i, row in enumerate(tiles):
            for col_i, tile in enumerate(row):
                result.paste(tile, box=(col_i * tile_size, row_i * tile_size))

        result.save(page_path)
        return result, page_path
    
    def scrape(self, zoom=0, delay=0.2):
        for i in tqdm(range(len(self)), unit='page'):
            self.page(i, zoom)
            # wait randomly 0.8-1.2x the specified delay
            sleep(delay * (random.random() + 4) / 5)

    @staticmethod
    def _get_view(permalink, page_i):
        response = requests.get(
            f'{permalink}&scan={page_i+1}'
        )

        logger.debug(f'Fetching view {page_i}:{response.status_code} ({response.url})')

        if response.status_code != 200:
            return None

        return html.fromstring(response.text)

    @staticmethod
    def _get_scan_path(view, scan_i):
        elements = view.xpath(f'//li[@id="scan{scan_i+1}"]/a/img')

        if len(elements) != 1:
            logger.debug(f'Scan {scan_i}: not found')
            return None

        path = elements[0].get('src')
        logger.debug(f'Scan {scan_i}: {path}')
        return path

    def _save(self):
        os.makedirs(self._dirname, exist_ok=True)
        with open(os.path.join(self._dirname, self.FILENAME), 'w') as book_f:
            dct = {k.lstrip('_'): v for k, v in vars(self).items() if (k != '_cache_dir')}
            json.dump(dct, book_f)

    def _get_tile(self, uuid, tile_group, zoom, col_i, row_i):
        url = f'{self._img_repo}/{uuid}.jpg/TileGroup{tile_group}/{zoom}-{col_i}-{row_i}.jpg'
        with urllib.request.urlopen(url) as tile_f:
            tile = Image.open(tile_f)
            tile.load()
        logger.debug(f'Fetching tile {uuid}[{tile_group},{zoom},{col_i},{row_i}] ({"x".join(map(str, tile.size))}px)')
        return tile
