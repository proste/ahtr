# Utility for fetching books from AHMP

## Installation

python and pip are expected to reference python 3.6+

```
# use virtual environment not to pollute system-wide installation
$ [python -m venv .env; . .env/bin/activate]
$ pip install -r requirements.txt
$ [deactivate]  # leave virtual environment
```

## Usage
```
$ python -i ahmp.py
>>> # fetch book
>>> book = Book.fetch(<permalink_to_book>, cache_dir=[<data_dir>])
>>> xid = book.xid
>>>
>>> # load already fetched book
>>> book = Book.load(xid)
>>>
>>> # load page image (cached to <data_dir>/<book_xid>/<zoom_level>-<page_i>-<page_id>.jpg)
>>> page_img = book.page(<page_i>, [zoom=<zoom_level>])
>>>
>>> # download all page images
>>> book.scrape([zoom=<zoom_level>, delay=<DoS_prevention>])
```

## FAQ
Where to find <permalink_to_book>?
> Visit http://katalog.ahmp.cz/pragapublica/ and navigate to document of your choice, here obtain the direct link to the document (link icon above preview)

What is zoom level?
> Each page is provided in several scales and resolutions, zoom=0 is the smallest one suitable for previewing pages, zoom=3 is often the largest one suitable for detailed inspection.
