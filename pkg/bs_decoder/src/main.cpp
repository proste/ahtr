#include <algorithm>
#include <cmath>
#include <codecvt>
#include <iostream>
#include <limits>
#include <locale>
#include <memory>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>
#include <pybind11/stl.h>

#include <lm/model.hh>
#include <lm/return.hh>
#include <lm/state.hh>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

float logsumexp(float a, float b) {
    if (a < b) { std::swap(a, b); }

    if (b == - std::numeric_limits<float>::infinity()) {
        return a;
    }

    return a + std::log1p(std::exp(b - a));
}

struct RayScore {
    float blank;
    float non_blank;

    float total() const { return logsumexp(blank, non_blank); }

    RayScore() :
        blank(- std::numeric_limits<float>::infinity()),
        non_blank(- std::numeric_limits<float>::infinity())
    { }

    RayScore(float blank, float non_blank) : blank(blank), non_blank(non_blank) { }
};

struct Ray {
    float extension_score;
    int n_words;
    lm::ngram::State char_state;
    lm::ngram::State word_state;
    std::u32string remainder;
    float remainder_score;
};

class RayScorer {
public:
    Ray extended_ray(const Ray & ray_, unsigned char char_idx) {
        Ray ray(ray_);
        ray.extension_score = 0.0f;

        // finalize remainder if requested and possible
        bool is_delimiter = (word_delimiters_.find(char_idx) != word_delimiters_.end());
        bool is_blank = (char_idx == blank_idx_);
        if (ray.remainder.size() && (is_delimiter || is_blank)) {
            bool is_oov;
            float word_score = word_extension_(
                ray_.word_state, ray.remainder, ray.word_state, ray.n_words, is_oov
            );

            // apply word score correction if possible
            if (!is_oov) {
                ray.extension_score += word_score - ray.remainder_score;
            }

            ray.remainder = U"";
            ray.remainder_score = 0.0f;
        }
        // process incoming char
        if (!is_blank) {
            char32_t char_ = charset_[char_idx];
            float char_score = char_extension_(
                ray_.char_state, char_, ray.char_state, ray.remainder.size()
            );

            if (char_ == U' ') {
            // extend with space (using char level model)
                ray.extension_score += char_score;
            } else if (is_delimiter) {
            // extend with delimiter (preferable using word level model)
                bool is_oov;
                lm::ngram::State word_state = ray.word_state;
                float word_score = word_extension_(
                    word_state,
                    std::u32string({ char_ }),
                    ray.word_state,
                    ray.n_words,
                    is_oov
                );
                ray.extension_score += (is_oov) ? char_score : word_score;
            } else {
            // extend remainder (using char level model)
                ray.extension_score += char_score;
                ray.remainder += char_;
                // TODO compute stats of remainder when extending
                // - is_oov, remainder_word_state, 
                ray.remainder_score += char_score;
            }
        }

        return ray;
    }

    Ray empty_ray() {
        Ray ray = {};
        char_lm_->NullContextWrite(&ray.char_state);
        word_lm_->NullContextWrite(&ray.word_state);
        return ray;
    }

    RayScorer(
        std::vector<char32_t> charset,
        unsigned char blank_idx,
        std::unordered_set<unsigned char> word_delimiters,
        char32_t space_sub,
        const std::string & char_lm_path,
        const std::string & word_lm_path,
        float alpha,
        float char_beta,
        float word_beta
    ) :
        charset_(charset),
        blank_idx_(blank_idx),
        word_delimiters_(word_delimiters),
        char_lm_(lm::ngram::LoadVirtual(char_lm_path.c_str())),
        word_lm_(lm::ngram::LoadVirtual(word_lm_path.c_str())),
        alpha_(alpha),
        char_beta_(char_beta),
        word_beta_(word_beta),
        prolong_ratios_()
    {
        space_sub_ = str_from_u32str_({ space_sub });
    }

private:
    std::vector<char32_t> charset_;
    unsigned char blank_idx_;
    std::unordered_set<unsigned char> word_delimiters_;
    std::string space_sub_;
    std::unique_ptr<lm::base::Model> char_lm_;
    std::unique_ptr<lm::base::Model> word_lm_;
    float alpha_;
    float char_beta_;
    float word_beta_;

    static constexpr float log10_e_ = std::log10(std::exp(1.0f));
    std::vector<float> prolong_ratios_;
    std::wstring_convert<std::codecvt_utf8<char32_t>, char32_t> str_conv_;

    float word_extension_(
        const lm::ngram::State & in_state,
        const std::u32string & token,
        lm::ngram::State & out_state,
        int & n_words,
        bool & is_oov
    ) {
        auto && token_idx = word_lm_->BaseVocabulary().Index(str_from_u32str_(token));
        auto && full_score = word_lm_->BaseFullScore(&in_state, token_idx, &out_state);

        is_oov = token_idx == 0;
        float word_score = (
            (full_score.prob / log10_e_) + (word_beta_ * get_prolong_ratio_(++n_words))
        );

        return word_score;
    }

    float char_extension_(
        const lm::ngram::State & in_state,
        char32_t char_,
        lm::ngram::State & out_state,
        int n_chars
    ) {
        std::string token = (char_ == U' ') ? space_sub_ : str_from_u32str_({ char_ });
        auto && token_idx = char_lm_->BaseVocabulary().Index(token);
        auto && full_score = char_lm_->BaseFullScore(&in_state, token_idx, &out_state);

        float char_score = (
            alpha_ * (
                (full_score.prob / log10_e_)
                + (char_beta_ * get_prolong_ratio_(++n_chars))
            )
        );

        return char_score;
    }

    float get_prolong_ratio_(int n) {
        for (int i = prolong_ratios_.size(); i < n; ++i) {
            prolong_ratios_.emplace_back(std::log(((float)(i + 2) / (i + 1))));
        }
        return prolong_ratios_[n - 1];
    }

    std::string str_from_u32str_(std::u32string str) {
        return str_conv_.to_bytes(str);
    }
};

std::vector<std::pair<std::string, RayScore>> focus_beam(
    std::unordered_map<std::string, RayScore> scores,
    py::ssize_t beam_width,
    bool use_cscore
) {
    std::vector<std::pair<std::string, RayScore>> beam(beam_width);
    std::partial_sort_copy(
        scores.begin(),
        scores.end(),
        beam.begin(),
        beam.end(),
        [=](
            std::pair<const std::string, const RayScore> const & l,
            std::pair<const std::string, const RayScore> const & r
        ) {
            if (use_cscore) {
                return (
                    (l.second.total() / l.first.size())
                    > (r.second.total() / r.first.size())
                );
            } else {
                return l.second.total() > r.second.total();
            }
        }
    );
    return beam;
}

std::vector<std::pair<std::vector<unsigned char>, float>> translate_beam(
    std::vector<std::pair<std::string, RayScore>> beam_scores
) {
    std::vector<std::pair<std::vector<unsigned char>, float>> result;
    for (auto && beam_score : beam_scores) {
        std::vector<unsigned char> ray_id(
            beam_score.first.begin(), beam_score.first.end()
        );
        result.emplace_back(std::move(ray_id), beam_score.second.total());
    };
    return result;
}

std::vector<std::pair<std::vector<unsigned char>, float>> decode(
    py::array_t<float> logprobs_np,
    unsigned char blank_idx,
    py::ssize_t beam_width,
    int pruning,
    RayScorer & ray_scorer,
    float gamma,
    bool use_cscore
) {
    auto logprobs = logprobs_np.unchecked<2>();
    py::ssize_t timesteps = logprobs.shape(0);
    py::ssize_t n_chars = logprobs.shape(1);
    float threshold = (pruning <= 0) ?
        (- std::numeric_limits<float>::infinity())
        : (- pruning * std::log(float(n_chars)));

    // init search state
    std::string init_ray;
    std::unordered_map<std::string, Ray> rays = {
        {init_ray, ray_scorer.empty_ray()},
    };
    std::unordered_map<std::string, RayScore> curr_scores = {
        { init_ray, RayScore(0, - std::numeric_limits<float>::infinity()) },
    };
    std::unordered_set<std::string> beam = { init_ray };

    // iterate over timesteps
    for (py::ssize_t timestep = 0; timestep < timesteps; ++timestep) {
        auto prev_scores = std::move(curr_scores);
        curr_scores = { };
        float blank_score = logprobs(timestep, blank_idx);

        // extend beam rays
        for (auto && ray_id_ : beam) {
            auto& ray_ = rays[ray_id_];
            auto& prev_score_ = prev_scores[ray_id_];
            auto& curr_score_ = curr_scores[ray_id_];

            // extend ray with each symbol
            for (unsigned char char_idx = 0; char_idx < n_chars; ++char_idx) {
                auto step_score = logprobs(timestep, char_idx);

                // append blank
                if (char_idx == blank_idx) {
                    curr_score_.blank = logsumexp(
                        curr_score_.blank, step_score + prev_score_.total()
                    );
                    continue;
                }

                if (step_score < threshold) {
                    continue;
                }

                // create extended ray
                auto ray_id = ray_id_ + std::string({ (char)(char_idx) });
                auto && ray_it = rays.find(ray_id);
                if (ray_it == rays.end()) {
                    ray_it = rays.insert({ray_id, ray_scorer.extended_ray(ray_, char_idx)}).first;
                }
                auto & ray = ray_it->second;
                auto extension_score = gamma * ray.extension_score;
                auto& curr_score = curr_scores[ray_id];

                if (ray_id_.size() && (char_idx == ray_id_[ray_id_.size() -1])) {
                // repeat non-blank
                    curr_score_.non_blank = logsumexp(
                        curr_score_.non_blank, step_score + prev_score_.non_blank
                    );
                    curr_score.non_blank = logsumexp(
                        curr_score.non_blank,
                        extension_score + step_score + prev_score_.blank
                    );
                } else {
                // append non-blank
                    curr_score.non_blank = logsumexp(
                        curr_score.non_blank,
                        extension_score + step_score + prev_score_.total()
                    );
                }

                // carry discarded rays
                auto prev_score_it = prev_scores.find(ray_id);
                if (
                    (beam.find(ray_id) == beam.end())
                    && (prev_score_it != prev_scores.end())
                ) {
                    auto& prev_score = prev_score_it->second;
                    curr_score.blank = logsumexp(
                        curr_score.blank, blank_score + prev_score.total()
                    );
                    curr_score.non_blank = logsumexp(
                        curr_score.non_blank, step_score + prev_score.non_blank
                    );
                }
            }
        }

        // drop abandoned rays
        for (auto it = rays.begin(); it != rays.end();) {
            auto curr_score_it = curr_scores.find(it->first);
            if (curr_score_it == curr_scores.end()) {
                it = rays.erase(it);
            } else {
                ++it;
            }
        }

        auto&& beam_scores = focus_beam(curr_scores, beam_width, use_cscore);
        beam.clear();
        for (auto && it : beam_scores) {
            beam.insert(std::move(it.first));
        }
    }

    // finalize rays
    for (auto && ray_id_ : beam) {
        auto& ray_ = rays[ray_id_];
        auto ray_id = ray_id_ + std::string({ (char)(blank_idx) });
        auto ray = ray_scorer.extended_ray(ray_, blank_idx);
        curr_scores[ray_id] = RayScore(
            - std::numeric_limits<float>::infinity(),
            gamma * ray.extension_score + curr_scores[ray_id_].total()
        );
    }

    return translate_beam(focus_beam(curr_scores, beam_width, use_cscore));
}

void test_scorer(std::vector<unsigned char> char_idxs, RayScorer & ray_scorer) {
    Ray ray = ray_scorer.empty_ray();
    py::print(ray.extension_score);
    for (auto && char_idx : char_idxs) {
        ray = ray_scorer.extended_ray(ray, char_idx);
        py::print(ray.extension_score, ray.remainder);
    }
}

PYBIND11_MODULE(bs_decoder, m) {
    m.doc() = R"pbdoc(
        Pybind11 example plugin
        -----------------------

        .. currentmodule:: bs_decoder

        .. autosummary::
           :toctree: _generate

           TODO
    )pbdoc";

    m.def("test_scorer", &test_scorer, "");

    py::class_<RayScorer>(m, "RayScorer")
        .def(py::init<>(
            [](
                std::vector<char32_t> charset,
                unsigned char blank_idx,
                std::unordered_set<unsigned char> word_delimiters,
                char32_t space_sub,
                const std::string & char_lm_path,
                const std::string & word_lm_path,
                float alpha,
                float char_beta,
                float word_beta
            ) {
                return std::unique_ptr<RayScorer>(new RayScorer(
                    charset,
                    blank_idx,
                    word_delimiters,
                    space_sub,
                    char_lm_path,
                    word_lm_path,
                    alpha,
                    char_beta,
                    word_beta
                ));
            }
        ));

    m.def("decode", &decode, R"pbdoc(
        decode
    )pbdoc");

    m.def("logsumexp", &logsumexp, "TODO");

#ifdef VERSION_INFO
    m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    m.attr("__version__") = "dev";
#endif
}
