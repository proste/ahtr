import os
from setuptools import setup

# Available at setup time due to pyproject.toml
from pybind11.setup_helpers import Pybind11Extension, build_ext
from pybind11 import get_cmake_dir

import sys

__version__ = "0.0.1"

# The main interface is through Pybind11Extension.
# * You can add cxx_std=11/14/17, and then build_ext can be removed.
# * You can set include_pybind11=false to add the include directory yourself,
#   say from a submodule.
#
# Note:
#   Sort input source files if you glob sources to ensure bit-for-bit
#   reproducible builds (https://github.com/pybind/python_example/pull/53)
KENLM_ROOT = os.environ.get('KENLM_ROOT', '/root/kenlm')
KENLM_ARGS = [
    '-DKENLM_MAX_ORDER=8',
    '-DHAVE_ZLIB',
    '-DHAVE_BZLIB',
    '-DHAVE_XZLIB',
    '-std=c++11'
]

KENLM_LIBS = ['stdc++', 'rt', 'z', 'bz2', 'lzma']

KENLM_OBJECTS = [
    f'{KENLM_ROOT}/build/lib/libkenlm.a',
    f'{KENLM_ROOT}/build/lib/libkenlm_util.a',
]

ARGS = ['-O3', '-DNDEBUG']
# ARGS = ['-O0', '-g3']  # debug build

LIBS = ['kenlm', 'kenlm_util']

ext_modules = [
    Pybind11Extension("bs_decoder",
        ["src/main.cpp"],
        define_macros = [('VERSION_INFO', __version__)],
        extra_compile_args=ARGS + KENLM_ARGS,
        include_dirs=[KENLM_ROOT],
        libraries=LIBS + KENLM_LIBS,
        library_dirs=[f'{KENLM_ROOT}/build/lib'],
    ),
]

setup(
    name="bs_decoder",
    version=__version__,
    author="Štěpán Procházka",
    author_email="prochazka.stepan@gmail.com",
    long_description="",
    ext_modules=ext_modules,
    extras_require={"test": "pytest"},
    # Currently, build_ext only provides an optional "highest supported C++
    # level" feature, but in the future it may provide more features.
    cmdclass={"build_ext": build_ext},
    zip_safe=False,
)
