import codecs
import locale
import os
import tempfile
from typing import Optional

import minio


class MinIOFile:
    def __init__(
        self,
        client: minio.Minio,
        bucket: str,
        key: str,
        mode: str,
        version_id: str = None,
        encoding=None,
        newline=None,
    ):
        assert mode in ('r', 'w', 'rt', 'wt', 'rb', 'wb')
        assert ('r' in mode) or (version_id is None)

        self._client = client
        self._bucket = bucket
        self._key = key
        self._mode = mode
        self._version_id = version_id
        self._encoding = encoding
        if not self._is_binary:
            self._encoding = self._encoding or 'utf-8'
        self._newline = newline
        self._tmp_f = None

    @property
    def _is_binary(self) -> bool:
        return 'b' in self._mode

    @property
    def bucket(self) -> str:
        return self._bucket

    @property
    def key(self) -> str:
        return self._key

    @property
    def version_id(self) -> Optional[str]:
        return self._version_id

    def __enter__(self):
        self._tmp_f = tempfile.NamedTemporaryFile(
            mode=('w+b' if self._is_binary else 'w+t'),
            encoding=self._encoding,
            newline=self._newline,
        )
        response = None
        if 'r' in self._mode:
            try:
                response = self._client.get_object(
                    self._bucket, self._key, version_id=self._version_id
                )

                chunks = response.stream()
                if not self._is_binary:
                    chunks = codecs.iterdecode(chunks, encoding=self._encoding)

                for chunk in chunks:
                    self._tmp_f.write(chunk)
            finally:
                if response is not None:
                    response.close()
                    response.release_conn()

            self._version_id = response.getheader('x-amz-version-id')
            self._tmp_f.seek(0)

        return self._tmp_f

    def __exit__(self, exc_type, exc_value, traceback):
        if (exc_type is None) and ('w' in self._mode):
            if self._is_binary:
                stream = self._tmp_f
            else:
                self._tmp_f.flush()
                stream = self._tmp_f.buffer

            stream.seek(0, 2)
            size = stream.tell()
            stream.seek(0)

            response = self._client.put_object(self._bucket, self._key, stream, size)
            self._version_id = response.version_id
        self._tmp_f.close()
        self._tmp_f = None

    def __repr__(self):
        return (
            f'{self.__class__.__name__}(bucket={self._bucket}, key={self._key}, '
            f'version_id={self._version_id}, mode={self._mode})'
        )


class Storage:
    def __init__(
        self,
        bucket: str = 'hawran',
        endpoint: str = None,
        access_key: str = None,
        secret_key: str = None,
        secure: bool = None,
        region: str = None,
    ):
        self._bucket = bucket
        self._endpoint = endpoint or os.environ['S3_ENDPOINT']
        self._access_key = access_key or os.environ.get('AWS_ACCESS_KEY_ID')
        self._secret_key = secret_key or os.environ.get('AWS_SECRET_ACCESS_KEY')
        self._secure = (
            secure if (secure is not None) else (os.environ.get('S3_USE_HTTPS', '1') != '0')
        )
        self._region = region or 'us-east-1'
        self._pid = None
        self._client = None

    def _normalize_key(self, key):
        if key.startswith('s3://'):
            _, _, bucket, key = key.split('/', maxsplit=3)
            if bucket != self._bucket:
                raise ValueError(f"Unexpected bucket {bucket} in path.")
        return key

    @property
    def client(self):
        pid = os.getpid()
        if (self._client is None) or (self._pid != pid):
            self._client = minio.Minio(
                self._endpoint,
                access_key=self._access_key,
                secret_key=self._secret_key,
                secure=self._secure,
                region=self._region,
            )
            self._pid = pid
        return self._client

    def open(
        self,
        key,
        mode,
        version_id=None,
        encoding=None,
        newline=None,
    ):
        return MinIOFile(
            self.client,
            self._bucket,
            self._normalize_key(key),
            mode,
            version_id=version_id,
            encoding=encoding,
            newline=newline,
        )

    def list(self, prefix):
        return {
            obj.object_name: obj
            for obj in sorted(
                self.client.list_objects(self._bucket, self._normalize_key(prefix)),
                key=(lambda obj: obj.object_name)
            )
        }


_STORAGE = Storage()


def open(key, mode, version_id=None, encoding=None, newline=None):
    return _STORAGE.open(
        key,
        mode,
        version_id=version_id,
        encoding=encoding,
        newline=newline,
    )


def list(prefix):
    return _STORAGE.list(prefix)
