import os
from pathlib import PurePath

ROOT_DIR = PurePath(os.path.normpath(os.path.join(os.path.dirname(__file__), '..')))
