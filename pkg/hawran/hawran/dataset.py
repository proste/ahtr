import itertools
import logging
import lzma
import re
import tarfile
from typing import Any, Callable, Iterable, Iterator, List, Mapping, Tuple, Union

import cv2
import numpy as np
import scipy.signal
from PIL import Image
from tqdm import tqdm

from hawran import storage, utils
from hawran.charset import Charset
from hawran.nprecord import NPRecord
from hawran.typesetting import (
    Font, HandwrittenFont, Inscription, InscriptionMetrics, SlabikarFont, TrueTypeFont,
    collate_inscriptions, spacify,
)
from hawran.utils import sliding_window


logger = logging.getLogger(__name__)


class Dataset:
    def __init__(self, seed=None):
        self._seed = seed
        self._rng = None

    @property
    def rng(self):
        if self._rng is None:
            self._rng = np.random.default_rng(self._seed)
        return self._rng

    def __iter__(self) -> Iterator[dict]:
        self._reset_state()
        for sample in self._iter_samples():
            yield from self._transform(**sample)

    def _reset_state(self):
        self._rng = None

    def _iter_samples(self) -> Iterator[dict]:
        raise NotImplementedError

    def _transform(self, **sample: dict) -> Iterator[dict]:
        yield sample


class TransformedDataset(Dataset):
    def __init__(self, source: Dataset, seed=None):
        super().__init__(seed=seed)
        self._source = source

    def _iter_samples(self):
        yield from self._source


class ListDataset(Dataset):
    @classmethod
    def from_components(cls, **components: Tuple[Any]):
        samples = [
            dict(zip(components.keys(), values))
            for values in zip(*components.values())
        ]
        return cls(samples)

    def __init__(self, samples: Iterable[dict]):
        super().__init__()
        self._samples = list(samples)

    def _iter_samples(self):
        yield from self._samples


class ShuffleDataset(TransformedDataset):
    @classmethod
    def maybe(cls, source: Dataset, buffer_len=None, seed=None):
        if seed is None:
            return source
        return cls(source, buffer_len, seed)

    def __init__(self, source: Dataset, buffer_len=None, seed=None):
        super().__init__(source=source, seed=seed)
        self._buffer_len = buffer_len

    def _random_idxs(self):
        while True:
            yield from self.rng.permutation(self._buffer_len)

    def _iter_samples(self):
        sample_it = iter(self._source)

        # allocate buffer
        logger.info(f"Allocating shuffle buffer ({self})...")
        if self._buffer_len is None:
            buffer = list(sample_it)
        else:
            buffer = [
                sample for _, sample in zip(range(self._buffer_len), sample_it)
            ]
        logger.info(f"Shuffle buffer allocated ({self})...")

        # yield & replace randomly until source exhausted
        for sample, idx in zip(sample_it, self._random_idxs()):
            yield buffer[idx]
            buffer[idx] = sample

        # exhaust remaining buffer
        for idx in self.rng.permutation(len(buffer)):
            yield buffer[idx]


class RepeatDataset(TransformedDataset):
    def __init__(self, source, reps=None):
        super().__init__(source=source)
        self._reps = reps

    def _iter_samples(self):
        if self._reps is None:
            while True:
                yield from self._source
        else:
            for _ in range(self._reps):
                yield from self._source


class FadeOutCache(TransformedDataset):
    def __init__(self, source: Dataset, reps: int, buffer_len: int):
        super().__init__(source=source)
        self._reps = reps
        self._buffer_len = buffer_len + ((- buffer_len) % self._reps)

    def _replace_idxs(self):
        while True:
            for offset in range(self._reps):
                for idx in range(offset, self._buffer_len, self._reps):
                    yield idx

    def _iter_samples(self):
        sample_it = iter(self._source)

        # allocate cache
        logger.info(f"Filling fade out cache ({self})..")
        head = []
        for _, sample in zip(range(self._buffer_len), sample_it):
            head.append(sample)
            yield sample
        logger.info(f"Fade out cache filled ({self})..")

        # simply repeat buffer in case of short source
        if len(head) < self._buffer_len:
            for _ in range(self._reps - 1):
                yield from head
                return

        # prepare buffer in right order
        buffer = [None] * self._buffer_len
        for idx, sample in zip(self._replace_idxs(), head):
            buffer[idx] = sample

        # fade out until source exhausted (reintroducing head at the end)
        replace_it = iter(self._replace_idxs())
        for idx, sample in zip(
            replace_it,
            itertools.chain(sample_it, head[:-(self._buffer_len // self._reps)]),
        ):
            buffer[idx] = sample
            page_idx = idx - (idx % self._reps)
            for sample_ in buffer[page_idx:(page_idx + self._reps)]:
                yield sample_


class InterleaveDataset(Dataset):
    def __init__(self, *sources: Dataset):
        super().__init__()
        self._sources = sources

    def _iter_samples(self):
        last_source_its = [iter(source) for source in self._sources]
        while last_source_its:
            next_source_its = []
            for source_it in last_source_its:
                try:
                    yield next(source_it)
                    next_source_its.append(source_it)
                except StopIteration:
                    pass
            last_source_its = next_source_its


class ZipDataset(Dataset):
    def __init__(self, *sources: Dataset):
        super().__init__()
        self._sources = sources

    def _iter_samples(self):
        for sample_parts in zip(*(iter(source) for source in self._sources)):
            result = {}
            for sample_part in sample_parts:
                assert not (sample_part.keys() & result.keys())
                result.update(sample_part)
            yield result


class HeadDataset(TransformedDataset):
    def __init__(self, source: Dataset, n: int, complement: bool = False):
        super().__init__(source)
        self._complement = complement
        self._n = n

    def _iter_samples(self):
        sample_it = iter(self._source)
        if self._complement:
            for _ in range(self._n):
                next(sample_it)

            yield from sample_it
        else:
            for _ in range(self._n):
                yield next(sample_it)


class SYNv4ShardTokensDataset(TransformedDataset):
    def __init__(self, shard_names: Dataset):
        super().__init__(source=shard_names)

    def _transform(self, shard_name: str) -> Iterator[dict]:
        def parse_line(line):
            line = line.rstrip('\n')
            line_id, *tokens = line.split('\t')
            block_id = ':'.join(line_id.split(':')[:2])
            return block_id, tokens

        with storage.open(shard_name, 'rb') as xz_f:
            with lzma.open(xz_f, 'rt') as shard_f:
                for block_id, tokens in utils.groupby(shard_f, key_val=parse_line):
                    tokens = list(itertools.chain.from_iterable(tokens))
                    yield {
                        'block_id': block_id,
                        'tokens': tokens,
                    }


class SpacifiedTokensDataset(TransformedDataset):
    def __init__(self, tokens: Dataset):
        super().__init__(source=tokens)

    def _transform(self, block_id: str, tokens: List[str]) -> Iterator[dict]:
        yield {
            'text_id': block_id,
            'text': spacify(tokens)
        }


class RandomLinesDataset(Dataset):
    def __init__(self, max_width: int, charset: Iterable[str], seed=None):
        super().__init__(seed=seed)
        self._max_width = max_width
        self._charset = np.asarray(charset)

        self._sample_count = None

    def _reset_state(self):
        super()._reset_state()
        self._sample_count = 0

    def _iter_samples(self):
        while True:
            yield {}

    def _transform(self) -> Iterator[dict]:
        idxs = self.rng.integers(len(self._charset), size=self._max_width)
        yield {
            'line_id': str(self._sample_count),
            'line': ''.join(self._charset[idxs]),
        }
        self._sample_count += 1


class GlyphsDataset(TransformedDataset):
    def __init__(self, scripts_ds: Dataset, height_px: int, charset: Charset):
        super().__init__(source=scripts_ds)

        self._height = height_px
        self._charset = charset

    def _transform(
        self, text_id: str, text: str, font_name: str, font: TrueTypeFont
    ) -> Iterator[dict]:
        _, size_pt, dpi = font.px_pt_dpi(px=self._height)
        chars = self._charset.retranscribe(text, has_char=font.has_char)
        glyphs = font.get_char_sequence(chars, size_pt, dpi)

        yield {
            'glyphs_id': f'{text_id};{font_name}',
            'glyphs': glyphs,
        }


class InscriptionsDataset(TransformedDataset):
    def __init__(
        self,
        glyphs_ds: Dataset,
        line_len: Union[int, Tuple[int, int]],
        preserve_words: bool,
        leading: int,
        collate_prob: float,
        seed=None,
    ):
        super().__init__(source=glyphs_ds, seed=seed)

        if isinstance(line_len, int):
            self._line_len = (line_len, line_len)
        else:
            self._line_len = tuple(line_len)
        self._preserve_words = preserve_words
        self._leading = leading
        self._collate_prob = collate_prob

    def _split_to_lines(self, glyphs):
        assert self._preserve_words

        advances, bearings, widths = zip(*(
            (g.metrics.advance[0], - g.metrics.bearing[0], g.bitmap.shape[1])
            for g in glyphs
        ))

        advances = np.array(advances, dtype=np.float32)
        bearings = np.array(bearings, dtype=np.float32)
        widths = np.array(widths, dtype=np.int32)

        preceeding_advance = np.r_[0, advances[:-1]].cumsum()
        raw_widths = preceeding_advance + widths - bearings

        begin = 0
        while begin < len(glyphs):
            line_len = self.rng.integers(*self._line_len, endpoint=True)
            offset = raw_widths[begin:].searchsorted(
                preceeding_advance[begin] + line_len - bearings[begin]
            )
            end = begin + offset

            if end == len(glyphs):
                yield glyphs[begin:end]
                begin = end
            else:
                for offset_, glyph in enumerate(glyphs[begin:(end + 1)][::-1]):
                    if glyph.transliteration == ' ':
                        end -= offset_
                        yield glyphs[begin:end]
                        begin = end + 1
                        break
                else:
                    yield glyphs[begin:end]
                    begin = end

    def _render_lines(self, lines):
        yield None
        for line_i, line in enumerate(lines):
            if (line_i != 0) and (self.rng.random() >= self._collate_prob):
                yield None

            # HACK very rarely long transliteration gets generated
            inscription = Inscription.join(line[:255])
            # HACK due to some bug, sometimes longer sequence is generated
            while inscription.bitmap.shape[1] > self._line_len[1]:
                logger.warning(
                    "Oversized line generated (%d > %d); shrinking",
                    inscription.bitmap.shape[1],
                    self._line_len[1],
                )
                line = line[:-1]
                inscription = Inscription.join(line)
            yield inscription
        yield None

    def _transform(self, glyphs_id: str, glyphs: List[Inscription]) -> Iterator[dict]:
        lines_it = self._split_to_lines(glyphs)

        for line_i, window in enumerate(
            sliding_window(self._render_lines(lines_it), size=3)
        ):
            if window[1] is None:
                continue

            collated_line = collate_inscriptions(
                *window,
                leading=self._leading,
                max_width=self._line_len[1],
                rng=self.rng,
            )

            yield {
                'inscription_id': glyphs_id,
                'inscription': collated_line,
            }


class InscriptionSamplesDataset(TransformedDataset):
    def __init__(
        self,
        inscriptions_ds: Dataset,
        height_px: int,
        horizontal_stride: int,
        seed,
    ):
        super().__init__(source=inscriptions_ds, seed=seed)
        self._height_px = height_px
        self._horizontal_stride = horizontal_stride
        # TODO add intensity scaling params and features

    def _transform(
        self, inscription_id: str, inscription: Inscription
    ) -> Iterator[dict]:
        img = inscription.bitmap
        x_height = inscription.metrics.x_height[1]
        h, w = img.shape
        w_padding = ((- w) % self._horizontal_stride)

        x = self.rng.integers(w_padding, endpoint=True)
        y = round(
            - inscription.metrics.bearing[1]
            + (inscription.metrics.advance[1] / 2)
            + self.rng.normal(x_height / 2, abs(x_height / 6))
            - (self._height_px // 2)
        )

        img_arr = np.zeros((self._height_px, w + w_padding), dtype=np.uint8)

        img = img[max(0, y):(y + self._height_px)]
        top = max(0, -y)
        img_arr[top:(top + img.shape[0]), x:(x + w)] = img

        yield {
            'example_id': inscription_id,
            'img': img_arr,
            'transliteration': inscription.transliteration,
        }


class HandwrittenFontsDataset(TransformedDataset):
    def __init__(
        self,
        font_names_ds: Dataset,
        rotation_range: Tuple[float, float],
        scale_x_range: Tuple[float, float],
        scale_y_range: Tuple[float, float],
        slant_range: Tuple[float, float],
        weight_range: Tuple[float, float],
        max_inconsistency: float,
        n_variants: int,
        supersampling: int = 4,
        seed=None,
    ):
        super().__init__(source=font_names_ds, seed=seed)

        begins, ends = zip(
            rotation_range, scale_x_range, scale_y_range, slant_range, weight_range
        )
        self._begins = np.array(begins, dtype=np.float32)
        self._ends = np.array(ends, dtype=np.float32)
        self._widths = self._ends - self._begins
        self._max_inconsistency = max_inconsistency
        self._n_variants = n_variants
        self._supersampling = supersampling

        self._font_cache = {}

    def _transform(self, font_name: str):
        font = self._get_font(font_name)

        inconsistency = self._max_inconsistency * self.rng.random()
        begins = self.rng.uniform(self._begins, self._ends)
        allowed_widths = inconsistency * self._widths
        ends = np.minimum(begins + allowed_widths, self._ends)
        rotation, scale_x, scale_y, slant, weight = zip(begins, ends)
        # HACK
        rotation = (- allowed_widths[0] / 2, allowed_widths[0] / 2)

        yield {
            'font_name': f'{font_name};handwritten',
            'font': HandwrittenFont(
                font,
                rotation,
                scale_x,
                scale_y,
                slant,
                weight,
                self._n_variants,
                self._supersampling,
                np.random.default_rng(self.rng.integers(2 ** 32)),
            ),
        }

    def _get_font(self, font_name: str):
        if font_name not in self._font_cache:
            with storage.open(font_name, 'rb') as font_f:
                if font_name.endswith('slabikar.ttf'):
                    self._font_cache[font_name] = SlabikarFont(font_f)
                else:
                    self._font_cache[font_name] = TrueTypeFont(font_f)
        return self._font_cache[font_name]


class RealPagesDataset(TransformedDataset):
    def __init__(self, archive_names_ds: Dataset):
        super().__init__(source=archive_names_ds)

    def _transform(self, archive_name: str) -> Iterator[dict]:
        with storage.open(archive_name, 'rb') as archive_f:
            with tarfile.open(fileobj=archive_f, mode='r') as tar_f:
                for member in tar_f:
                    img = Image.open(tar_f.extractfile(member))

                    yield {
                        'page_id': f'{archive_name};{member.name}',
                        'page': self._preprocess(img),
                    }

    @staticmethod
    def _preprocess(img, block_size=13, C=32, kernel=None):
        img_arr = np.asarray(img)
        gray_img = cv2.cvtColor(img_arr, cv2.COLOR_RGB2GRAY)

        # remove background noise
        thresholded = cv2.adaptiveThreshold(
            gray_img, 1, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, block_size, C
        )
        mask = (
            1 - cv2.erode(thresholded, kernel or np.ones((3, 5), np.uint8))
        ).astype(np.bool)
        preprocessed_img = (- gray_img * mask)

        # cut horizontal margins
        vertical_intensity = preprocessed_img.sum(axis=0) / preprocessed_img.shape[0]
        fg_mask = np.r_[
            False, (2 * vertical_intensity) > vertical_intensity.mean(), False
        ]

        if fg_mask.any():
            begins = np.flatnonzero(fg_mask[:-2] < fg_mask[1:-1])
            ends = np.flatnonzero(fg_mask[1:-1] > fg_mask[2:]) + 1
            longest_run_idx = np.argmax(ends - begins)
            offset = round(preprocessed_img.shape[1] / 64)
            begin = max(0, begins[longest_run_idx] - offset)
            end = min(preprocessed_img.shape[1], ends[longest_run_idx] + offset)
            preprocessed_img = preprocessed_img[:, begin:end]

        return preprocessed_img


class RealSamplesDataset(TransformedDataset):
    def __init__(self, pages_ds: Dataset, height: int):
        super().__init__(source=pages_ds)
        self._height = height

    @staticmethod
    def _get_midlines(img):
        mean = img.mean()
        contours = ((img[:, :-1] < mean) & (img[:, 1:] >= mean))
        counts = contours.sum(axis=-1)
        rel_counts = counts / max(1, counts.max())
        peaks_, peak_properties_ = scipy.signal.find_peaks(rel_counts, prominence=0.42)

        if not peaks_.size:
            return peaks_

        distance = 0.666 * np.quantile(
            peak_properties_['right_bases'] - peak_properties_['left_bases'], 0.05
        )
        width = 0.42 * np.quantile(
            scipy.signal.peak_widths(
                rel_counts, peaks_, prominence_data=peak_properties_.values()
            )[0],
            0.05
        )

        peaks, _ = scipy.signal.find_peaks(
            rel_counts, prominence=0.125, distance=distance, width=width
        )
        return peaks

    def _transform(self, page_id: str, page: np.ndarray) -> Iterator[dict]:
        midlines = self._get_midlines(page)

        if len(midlines) < 2:
            # TODO debug log
            return

        max_gap = round(np.quantile(np.diff(midlines), 0.9))
        begins = np.r_[0, midlines[:-1]]
        ends = np.r_[midlines[1:], page.shape[0]]

        w = page.shape[1]
        for line_i, (begin, mid, end) in enumerate(zip(begins, midlines, ends)):
            asc_, desc_ = mid - begin, end - mid
            var = 1.1
            asc = min(asc_, round(desc_ * var), max_gap)
            desc = min(desc_, round(asc_ * var), max_gap)
            h = asc + desc
            scale = self._height / h
            line = np.asarray(
                Image.fromarray(page[(mid - asc):(mid + desc)]).resize(
                    (round(scale * w), round(scale * h)), Image.BILINEAR, reducing_gap=2
                )
            )

            yield {
                'example_id': f'{page_id};{line_i}',
                'img': line,
            }


class DatasetSplit(Dataset):
    def __init__(self, k: int, n: int, source: Dataset):
        super().__init__()
        self._k = k
        self._n = n
        self._source = source

    def _iter_samples(self):
        for sample_i, sample in enumerate(self._source):
            if (sample_i % self._n) == self._k:
                yield sample


class FilterComponentsDataset(TransformedDataset):
    def __init__(
        self,
        source: Dataset,
        keep: Iterable[str] = (),
        drop: Iterable[str] = (),
    ):
        super().__init__(source)
        self._keep = tuple(keep)
        self._drop = tuple(drop)

    def _transform(self, **components) -> Iterator[dict]:
        if self._keep:
            yield {key: value for key, value in components.items() if key in self._keep}
        elif self._drop:
            yield {
                key: value
                for key, value in components.items()
                if key not in self._drop
            }
        else:
            yield components


class RenameComponentsDataset(TransformedDataset):
    def __init__(self, ds: Dataset, mapping: Mapping[str, str]):
        super().__init__(ds)
        mapping = dict(mapping)
        if len(mapping.values()) != len(set(mapping.values())):
            raise ValueError('Invalid name mapping, some values duplicate')
        self._mapping = mapping

    def _transform(self, **components) -> dict:
        yield {
            self._mapping.get(key, key): value
            for key, value in components.items()
        }


class CacheDataset(Dataset):
    @classmethod
    def from_file(cls, path: str):
        return cls(None, path)

    @classmethod
    def maybe(cls, ds: Dataset, path: str):
        if not path:
            return ds
        return cls(ds, path)

    def __init__(self, ds: Dataset, path: str):
        self._source = ds
        self._path = path

        self._cached = (ds is None)
        self._npr_f = None

    def cache(self, verbose: bool = True):
        if not self._cached:
            for _ in tqdm(self, disable=(not verbose)):
                pass

    def _iter_samples(self):
        if not self._cached:
            with storage.open(self._path, 'wb') as npr_f:
                with NPRecord(npr_f, 'w') as npr:
                    for sample in self._source:
                        npr.append(sample)
                        yield sample
            self._cached = True
        else:
            self._npr_f = self._npr_f or storage.open(self._path, 'rb').__enter__()
            with NPRecord(self._npr_f.name, 'r') as npr:
                yield from npr

    def __del__(self):
        if self._npr_f is not None:
            self._npr_f.__exit__(None, None, None)
