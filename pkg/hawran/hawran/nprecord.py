import io
import json
import zipfile
from typing import Dict

import numpy as np


class NPRecord:
    def __init__(self, file, mode: str):
        assert mode in ('r', 'w')

        self._file = file
        self._mode = mode

        self._header = None
        self._n_records = None
        self._zipfile = None

    def append(self, components: Dict[str, np.ndarray]):
        assert self._is_open
        assert self._mode == 'w'

        if self._header is None:
            self._header = frozenset(components.keys())
        else:
            assert set(components.keys()) == self._header

        record_idx = self._n_records
        for name, value in components.items():
            arcname = self._member_name(record_idx, name)
            with self._zipfile.open(arcname, 'w') as npy_f:
                np.save(npy_f, value)
        self._n_records += 1

        return record_idx

    def __iter__(self):
        assert self._is_open
        for i in range(self._n_records):
            yield self._load_record(i)

    def __getitem__(self, key):
        if isinstance(key, int) and (0 <= key < self._n_records):
            return self._load_record(key)
        raise IndexError

    def __enter__(self):
        assert not self._is_open

        self._zipfile = zipfile.ZipFile(
            self._file, compression=zipfile.ZIP_LZMA, mode=self._mode
        )
        self._zipfile.__enter__()

        if self._mode == 'r':
            comment = json.loads(self._zipfile.comment)
            self._header = frozenset(comment['header'])
            self._n_records = comment['n_records']
        else:  # mode == 'w'
            self._header = None
            self._n_records = 0

        assert self._is_open
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            if self._zipfile is not None:
                return self._zipfile.__exit__(exc_type, exc_value, traceback)

        assert self._is_open

        if self._mode == 'w':
            comment = {
                'header': sorted(self._header),
                'n_records': self._n_records,
            }
            self._zipfile.comment = json.dumps(comment).encode('utf-8')

        zipfile = self._zipfile
        self._header = self._n_records = self._zipfile = None

        assert not self._is_open
        return zipfile.__exit__(exc_type, exc_value, traceback)

    @property
    def _is_open(self):
        return None not in (self._n_records, self._zipfile)

    def _load_record(self, record_idx):
        record = {}
        for name in self._header:
            arcname = self._member_name(record_idx, name)
            npy_f = io.BytesIO(self._zipfile.read(arcname))
            member = np.load(npy_f)
            if member.ndim == 0:
                member = member.item()
            record[name] = member
        return record

    @staticmethod
    def _member_name(record_idx, component_name):
        return f'{record_idx}/{component_name}.npy'
