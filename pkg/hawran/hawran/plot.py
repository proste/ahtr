import io

from matplotlib import cm


def heatmap(values, ax, cmap, vmin=None, vmax=None, fmt=None):
    fmt = fmt or '{}'

    vmin = values.min() if (vmin is None) else vmin
    vmax = values.max() if (vmax is None) else vmax
    if vmax <= vmin:
        vmax = vmin + 1

    ax.imshow(values, cmap=cmap, aspect='auto', vmin=vmin, vmax=vmax)

    min_val = values.min() if (vmin is None) else vmin
    max_val = values.max() if (vmax is None) else vmax
    norm_values = (values - min_val) / (max_val - min_val)
    cmap_ = cm.get_cmap(cmap)
    low, high = cmap_(0), cmap_(cmap_.N)
    for row_i, row in enumerate(values):
        for col_i, cell in enumerate(row):
            if cell:
                color = low if (norm_values[row_i, col_i] > 0.5) else high
                if vmin <= cell <= vmax:
                    _ = ax.text(
                        col_i, row_i, fmt.format(cell), ha="center", va="center", color=color,
                    )


def fig_to_image(figure):
    buffer = io.BytesIO()
    figure.savefig(buffer, format='png', dpi='figure')
    buffer.seek(0)

    return buffer.getvalue()
