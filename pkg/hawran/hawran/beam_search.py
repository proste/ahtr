import itertools
from collections import defaultdict, deque
from typing import Generic, Iterable, NamedTuple, Tuple, TypeVar

import kenlm
import numpy as np
import scipy

from hawran.charset import Charset
from hawran.language_modelling import LanguageModel


RayT = TypeVar('RayT', bound=tuple)


class RayScorerBase(Generic[RayT]):
    @property
    def empty_ray(self):
        raise NotImplementedError

    def extended_ray(self, ray_: RayT, char_idx: int) -> RayT:
        raise NotImplementedError

class BeamSearchDecoder:
    class RayScore:
        def __init__(self, blank: float = 0, non_blank: float = 0):
            self.blank = blank
            self.non_blank = non_blank

        @property
        def total(self):
            return self.blank + self.non_blank

        def __repr__(self):
            return (
                f'{self.__class__.__name__}('
                f'blank={self.blank}, non_blank={self.non_blank})'
            )

    def __init__(self, n_rays: int, blank_idx: int, ray_scorer: RayScorerBase):
        self._n_rays = n_rays
        self._blank_idx = blank_idx
        self._ray_scorer = ray_scorer

    def decode(self, logits, pruning=None):
        # TODO consider using logprobs
        step_scores = scipy.special.softmax(logits.astype(np.float64), axis=-1)
        timesteps, n_chars = step_scores.shape
        threshold = 0 if (pruning is None) else (n_chars ** (- pruning))

        init_ray_id = ()
        rays = {init_ray_id: self._ray_scorer.empty_ray}
        curr_scores = defaultdict(self.RayScore)
        curr_scores[init_ray_id] = self.RayScore(1, 0)
        beam = {init_ray_id}

        for t in range(timesteps):
            prev_scores, curr_scores = curr_scores, defaultdict(self.RayScore)
            blank_score = step_scores[t, self._blank_idx]

            # extend promising rays
            for ray_id_ in beam:
                ray_ = rays[ray_id_]
                prev_score_ = prev_scores[ray_id_]
                curr_score_ = curr_scores[ray_id_]
                for char_idx, step_score in enumerate(step_scores[t]):
                    # append blank
                    if char_idx == self._blank_idx:
                        curr_score_.blank += step_score * prev_score_.total
                        continue

                    if step_score < threshold:
                        continue

                    # create extended ray
                    ray_id = ray_id_ + (char_idx,)
                    ray = rays[ray_id] = self._ray_scorer.extended_ray(ray_, char_idx)
                    curr_score = curr_scores[ray_id]

                    # repeat non-blank
                    if ray_id_ and (char_idx == ray_id_[-1]):
                        curr_score_.non_blank += step_score * prev_score_.non_blank
                        curr_score.non_blank += (
                            ray.extension_score * step_score * prev_score_.blank
                        )
                    # append non-blank
                    else:
                        curr_score.non_blank += (
                            ray.extension_score * step_score * prev_score_.total
                        )

                    # carry discarded rays
                    if (ray_id not in beam) and (ray_id in prev_scores):
                        prev_score = prev_scores[ray_id]
                        curr_score.blank += blank_score * prev_score.total
                        curr_score.non_blank += step_score * prev_score.non_blank

            # drop abandoned rays
            for id_ in (prev_scores.keys() - curr_scores.keys()):
                del rays[id_]

            beam = frozenset(self._focus_beam(curr_scores))

        for ray_id_ in beam:
            ray_ = rays[ray_id_]
            ray_id = ray_id_ + (self._blank_idx,)
            ray = self._ray_scorer.extended_ray(ray_, self._blank_idx)
            curr_scores[ray_id] = self.RayScore(
                0, ray.extension_score * curr_scores[ray_id_].total
            )

        return [
            (ray_id, curr_scores[ray_id].total)
            for ray_id in self._focus_beam(curr_scores)
        ]

    def _focus_beam(self, scores):
        ray_ids, ray_scores = zip(*(
            (ray_id, score.total) for ray_id, score in scores.items()
        ))
        ray_scores = np.array(ray_scores)

        best_idxs = np.argsort(ray_scores)[::-1][:self._n_rays]
        mask = ray_scores[best_idxs] > 0.0

        return [ray_ids[idx] for idx in best_idxs[mask]]


class DummyRayScorer(RayScorerBase['DummyRayScorer.Ray']):
    class DummyRay(NamedTuple):
        extension_score: float = 1.0

    def __init__(self):
        self._dummy_ray = DummyRay()

    @property
    def empty_ray(self):
        return self._dummy_ray

    def extended_ray(self, ray_: None, char_idx: int) -> None:
        return self._dummy_ray


class LMRayScorer(RayScorerBase['LMRayScorer.Ray']):
    class Ray(NamedTuple):
        extension_score: float
        n_words: int
        char_state: kenlm.State
        word_state: kenlm.State
        remainder: str
        remainder_score: str

    def __init__(
        self,
        charset: Charset,
        word_delimiter_idxs: Iterable[int],
        char_lm: LanguageModel,
        char_alpha: float,
        char_beta: float,
        word_lm: LanguageModel,
        word_alpha: float,
        word_beta: float,
    ):
        super().__init__()

        self._charset = charset
        self._word_delimiters = frozenset(word_delimiter_idxs)
        self._char_lm = char_lm
        self._char_alpha = char_alpha
        self._char_beta = char_beta
        self._word_lm = word_lm
        self._word_alpha = word_alpha
        self._word_beta = word_beta

        self._prolong_ratio = np.r_[
            np.nan,
            np.log10(
                np.arange(2, 256, dtype=np.float32)
                / np.arange(1, 255, dtype=np.float32)
            ),
        ].tolist()

    @property
    def empty_ray(self):
        return self.Ray(
            extension_score=1.0,
            n_words=0,
            char_state=self._char_lm.empty_state,
            word_state=self._word_lm.empty_state,
            remainder='',
            remainder_score=0.0,
        )

    def extended_ray(self, ray_: RayT, char_idx: int) -> RayT:
        extension_score = 0.0

        # finalize remainder if requested and possible
        is_delimiter = char_idx in self._word_delimiters
        is_blank = char_idx == self._charset.blank_idx
        remainder = ray_.remainder
        remainder_score = ray_.remainder_score
        word_state = ray_.word_state
        n_words = ray_.n_words
        if (remainder and (is_delimiter or is_blank)):
            word_state, n_words, word_score = self._word_extension(
                word_state, n_words, remainder
            )

            # apply word score correction if possible
            if word_score is not None:
                extension_score += word_score - remainder_score

            remainder = ''
            remainder_score = 0.0

        # process incoming char
        char_state = ray_.char_state
        if not is_blank:
            char = str(self._charset._charset[char_idx])
            char_state, char_score = self._char_extension(
                char_state, len(remainder), char
            )

            # extend with blank (using char level model)
            if char == ' ':
                extension_score += char_score
            # extend with delimiter (preferably using word level model)
            elif is_delimiter:
                word_state, n_words, word_score = self._word_extension(
                    word_state, n_words, char
                )
                extension_score += char_score if (word_score is None) else word_score
            # extend remainder (using char level model)
            else:
                extension_score += char_score
                remainder += char
                remainder_score += char_score

        return self.Ray(
            extension_score=(10 ** extension_score),
            n_words=n_words,
            char_state=char_state,
            word_state=word_state,
            remainder=remainder,
            remainder_score=remainder_score,
        )

    def _word_extension(self, word_state, n_words, word):
        n_words += 1
        word_logprob, word_state, is_oov = self._word_lm(word_state, word)

        word_score = None if is_oov else (
            (self._word_alpha * word_logprob)
            + (self._word_beta * self._prolong_ratio[n_words])
        )

        return word_state, n_words, word_score

    def _char_extension(self, char_state, n_chars, char):
        n_chars += 1
        char_logprob, char_state, is_ooc = self._char_lm(char_state, char)
        assert not is_ooc

        char_score = (
            (self._char_alpha * char_logprob)
            + (self._char_beta * self._prolong_ratio[n_chars])
        )

        return char_state, char_score
