import itertools
import logging
import re
import unicodedata
from functools import lru_cache
from typing import Any, Hashable, Iterable, List, Optional, Tuple, Union

import freetype
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image, ImageFilter

from hawran.charset import syn_v4_charset
from hawran.utils import sliding_window


logger = logging.getLogger(__name__)


class InscriptionMetrics:
    def __init__(
        self,
        advance: Tuple[float, float],
        bearing: Tuple[float, float],
        ascender_size: float,
        descender_size: float,
        x_height: float,
    ):
        self.advance = advance
        self.bearing = bearing
        self.ascender_size = ascender_size
        self.descender_size = descender_size
        self.x_height_size = x_height

        self._up_unit = None
        self._ascender = None
        self._descender = None
        self._x_height = None

    def __repr__(self):
        return (
            f"{self.__class__.__name__}("
            f"advance={self.advance},"
            f"bearing={self.bearing},"
            f"ascender_size={self.ascender_size},"
            f"descender_size={self.descender_size},"
            f"x_height={self.x_height_size}"
            ")"
        )

    @staticmethod
    def _compute_up_unit(direction):
        if (np.asarray(direction) == 0).all():
            return np.array([0, -1], dtype=np.float32)

        up = np.array([direction[1], - direction[0]])
        return up / np.sqrt((up ** 2).sum())

    @property
    def up_unit(self):
        if self._up_unit is None:
            self._up_unit = self._compute_up_unit(self.advance)
        return self._up_unit

    @property
    def ascender(self):
        if self._ascender is None:
            self._ascender = self.ascender_size * self.up_unit
        return self._ascender

    @property
    def descender(self):
        if self._descender is None:
            self._descender = - self.descender_size * self.up_unit
        return self._descender

    @property
    def x_height(self):
        if self._x_height is None:
            self._x_height = self.x_height_size * self.up_unit
        return self._x_height

    def transformed(self, transformation: np.ndarray) -> 'InscriptionMetrics':
        cursor = - np.asarray(self.bearing)
        advance = np.asarray(self.advance)

        mid_baseline = cursor + (advance) / 2

        up_unit = mid_baseline + self.up_unit
        key_points = np.pad(
            np.array([
                cursor,
                cursor + advance,
                mid_baseline,
                mid_baseline + self.up_unit,
            ]),
            ((0, 0), (0, 1)),
            constant_values=1,
        )

        key_points_ = np.matmul(transformation, key_points.T)[:2].T
        bearing_ = - key_points_[0]
        advance_, mid_baseline_, mid_unit_ = key_points_[1:] + bearing_

        transformed_up_unit_ = mid_unit_ - mid_baseline_
        true_up_unit_ = self._compute_up_unit(advance_)
        scale = (transformed_up_unit_ * true_up_unit_).sum()


        return self.__class__(
            tuple(advance_.tolist()),
            tuple(bearing_.tolist()),
            scale * self.ascender_size,
            scale * self.descender_size,
            scale * self.x_height_size,
        )

    @classmethod
    def join(
        cls,
        items: Iterable['InscriptionMetrics'],
        return_left_tops: bool = False,
    ) -> 'InscriptionMetrics':
        items = list(items)

        advances = [(0, 0)] + [item.advance for item in items]
        cumulative_advances = np.cumsum(advances, axis=0, dtype=np.float32)
        cursors, joint_advance = cumulative_advances[:-1], cumulative_advances[-1]
        bearings = np.array([item.bearing for item in items], dtype=np.float32)

        left_tops = cursors + bearings
        joint_bearing = left_tops.min(axis=0)

        up_unit = cls._compute_up_unit(joint_advance)
        scales = [
            (up_unit * item.up_unit).sum()
            for item in items
        ]

        # median ascender, descender and x_height
        joint_ascender, joint_descender, joint_x_height = np.median(
            [
                (
                    scale * item.ascender_size,
                    scale * item.descender_size,
                    scale * item.x_height_size,
                )
                for item, scale in zip(items, scales)
            ],
            axis=0,
        )

        result = [cls(
            joint_advance,
            joint_bearing,
            joint_ascender,
            joint_descender,
            joint_x_height
        )]

        if return_left_tops:
            result.append(left_tops)

        return result

class Inscription:
    def __init__(
        self,
        bitmap: np.ndarray,
        metrics: InscriptionMetrics,
        annotation: np.ndarray,
        transliteration: str,
    ):
        # TODO(proste) implement dynamic type size selection to allow longer transliteration
        assert len(transliteration) < 256
        self.bitmap = bitmap
        self.metrics = metrics
        self.annotation = annotation
        self.transliteration = transliteration

    def inspect(self, ax=None):
        fg_mask = self.annotation.astype(np.bool)
        odd_mask = (self.annotation % 2).astype(np.bool)
        canvas = np.maximum.reduce([
            32 * odd_mask * fg_mask,
            64 * (~odd_mask) * fg_mask,
            self.bitmap
        ])

        baseline = - self.metrics.bearing[1] + (self.metrics.advance[1] / 2)
        for hline in (
            baseline,
            baseline + self.metrics.x_height[1],
            baseline + self.metrics.ascender[1],
            baseline + self.metrics.descender[1],
        ):
            hline = round(float(hline))
            if 0 <= hline < len(canvas):
                canvas[round(float(hline)), :] = 255

        (ax or plt).imshow(canvas, vmin=0, vmax=255)

        return {
            'transliteration': self.transliteration,
            'shape': self.bitmap.shape,
            'metrics': self.metrics,
        }

    def transformed(self, transformation: np.ndarray) -> 'Inscription':
        h, w = self.bitmap.shape
        corners = np.matmul(transformation, [[w, w, 0, 0], [0, h, h, 0], [1, 1, 1, 1]])
        left_top = corners.min(axis=-1)[:2]
        right_bottom = corners.max(axis=-1)[:2]
        size = tuple(np.ceil(right_bottom - left_top).astype(np.int32))

        # transform canvas (as tightly as possible)
        transformation = transformation.copy()
        transformation[:2, 2] -= left_top
        inv_transformation = np.linalg.inv(transformation)

        distorted_bitmap = np.asarray(
            Image.fromarray(self.bitmap).transform(
                size,
                method=Image.AFFINE,
                data=inv_transformation[:2].ravel(),
                resample=Image.BILINEAR,
            )
        )

        distorted_annotation = np.asarray(
            Image.fromarray(self.annotation).transform(
                size,
                method=Image.AFFINE,
                data=inv_transformation[:2].ravel(),
                resample=Image.NEAREST,
            )
        )

        distorted_metrics = self.metrics.transformed(transformation)

        return self.__class__(
            distorted_bitmap,
            distorted_metrics,
            distorted_annotation,
            self.transliteration,
        )

    @classmethod
    def join(cls, items: Iterable['Inscription']) -> 'Inscription':
        items = list(items)

        joint_metrics, left_tops = InscriptionMetrics.join(
            [item.metrics for item in items], return_left_tops=True
        )
        left_tops = np.round(left_tops).astype(np.int32)
        sizes = np.array([item.bitmap.shape[::-1] for item in items], dtype=np.int32)
        right_bottoms = left_tops + sizes
        bbox = np.r_[left_tops.min(axis=0), right_bottoms.max(axis=0)]
        size = bbox[2:] - bbox[:2]

        bitmap_canvas = np.zeros(size[::-1], dtype=np.uint8)
        annotation_canvas = np.zeros(size[::-1], dtype=np.uint8)

        placements = np.concatenate(
            (left_tops - bbox[:2], right_bottoms - bbox[:2]),
            axis=1
        )
        for item, (l, t, r, b) in zip(items, placements):
            bitmap_canvas[t:b, l:r] = np.maximum(item.bitmap, bitmap_canvas[t:b, l:r])
            annotation_canvas[t:b, l:r] = np.maximum(
                (item.annotation != 0) * (item.annotation + annotation_canvas.max()),
                annotation_canvas[t:b, l:r]
            )

        return cls(
            bitmap_canvas,
            joint_metrics,
            annotation_canvas,
            ''.join(item.transliteration for item in items),
        )

    def __add__(self, other: 'Inscription') -> 'Inscription':
        if self.__class__ is not other.__class__:
            return NotImplemented
        return self.join((self, other))

    def extended(self, others: Iterable['Inscription']) -> 'Inscription':
        return self.join((self, *others))


class Font:
    _DPI = 150
    _PT_PER_INCH = 72

    @classmethod
    def px_pt_dpi(cls, *, px: int = None, pt: float = None, dpi: int = None):
        assert (pt is None) != (px is None)
        dpi = dpi or cls._DPI

        if px is None:
            px = dpi * (pt / cls._PT_PER_INCH)
        else:  # pt is None
            pt = cls._PT_PER_INCH * (px / dpi)

        return px, pt, dpi

    def has_char(self, char: str) -> bool:
        raise NotImplementedError

    def get_char(self, char: str, size_pt: float, dpi: int = None) -> Inscription:
        # TODO consider making copy here (or at least readonly views)
        return self._get_char(None, char, None, size_pt, dpi or self._DPI)

    def get_char_sequence(self, chars: str, size_pt: float, dpi: int = None) -> List[Inscription]:
        return [
            self._get_char(prev_c, curr_c, succ_c, size_pt, dpi or self._DPI)
            for prev_c, curr_c, succ_c in sliding_window(
                itertools.chain([None], chars, [None]), size=3
            )
        ]

    def _get_glyph_id(
        self,
        prev_c: Optional[str],
        curr_c: str,
        succ_c: Optional[str],
        size_pt: float,
        dpi: int,
    ) -> Tuple[Hashable, ...]:
        return (curr_c, size_pt, dpi)

    def _get_glyph(self, *glyph_id: Hashable) -> Inscription:
        NotImplemented

    def _get_char(
        self,
        prev_c: Optional[str],
        curr_c: str,
        succ_c: Optional[str],
        size_pt: float,
        dpi: int,
    ) -> Inscription:
        glyph_id = self._get_glyph_id(prev_c, curr_c, succ_c, size_pt, dpi)
        glyph = self._get_glyph(*glyph_id)
        return glyph


class TrueTypeFont(Font):
    _X_HEIGHT_CHAR = 'e'

    def __init__(self, font_path):
        super().__init__()

        self.font = freetype.Face(font_path)

        ref_pt = round(self._PT_PER_INCH * 64)
        reference_chars = [
            self.__render_glyph(g, ref_pt)
            for g in syn_v4_charset.out_chars if self.has_char(g)
        ]

        max_ascender = max(abs(char[2][1]) for char in reference_chars)
        max_descender = max((char[0].shape[0] + char[2][1]) for char in reference_chars)
        max_height = max_ascender + max_descender

        self._baseline_ratio = (max_ascender / max_height)
        self._scale_correction = self._DPI / max_height

    def has_char(self, char: str) -> bool:
        return self.font.get_char_index(char) != 0

    def x_height(self, size_pt: float, dpi: int = None) -> float:
        return self._x_height(self._to_normalized_26_6(size_pt, dpi or self._DPI))

    def _to_normalized_26_6(self, size_pt: float, dpi: int) -> int:
        return round(self._scale_correction * size_pt * (dpi / self._DPI) * 64)

    @lru_cache(maxsize=None)
    def _get_metrics(self, normalized_size_26_6: int):
        px, *_ = self.px_pt_dpi(
            pt=(normalized_size_26_6 / (self._scale_correction * 64)),
            dpi=self._DPI,
        )
        ascender_size = px * self._baseline_ratio
        descender_size = px - ascender_size
        x_height = self._x_height(normalized_size_26_6)

        return ascender_size, descender_size, x_height

    def _get_glyph_id(
        self, prev_c: Optional[str], curr_c: str, succ_c: Optional[str], size_pt: float, dpi: int
    ) -> Tuple[Hashable, ...]:
        return (curr_c, self._to_normalized_26_6(size_pt, dpi))

    @lru_cache(maxsize=None)
    def _x_height(self, normalized_size_26_6: int):
        return - round(
            self.__render_glyph(self._X_HEIGHT_CHAR, normalized_size_26_6)[2][1]
        )

    def __render_glyph(self, char, normalized_size_26_6):
        self.font.set_char_size(
            height=normalized_size_26_6, hres=self._DPI, vres=self._DPI
        )

        normalized_char = unicodedata.normalize('NFD', char)
        ex = None
        for retry in range(2):
            try:
                if len(normalized_char) > 1:
                    self.font.load_char(normalized_char[0])
                    horiAdvance = self.font.glyph.metrics.horiAdvance / 64
                    self.font.load_char(char)
                else:
                    self.font.load_char(char)
                    horiAdvance = self.font.glyph.metrics.horiAdvance / 64
                break
            except freetype.FT_Exception as ex:
                logger.warning(
                    f"FT Exception raised with font={self.font.family_name}, "
                    f"char={char!r}, normalized_size_26_6={normalized_size_26_6}."
                )
        else:
            raise RuntimeError from ex

        glyph = self.font.glyph

        metrics = glyph.metrics
        advance = (horiAdvance, 0)

        bitmap = glyph.bitmap
        if len(bitmap.buffer):
            arr = np.array(bitmap.buffer, dtype=np.uint8).reshape(-1, bitmap.width)
            bearing = (metrics.horiBearingX / 64, - metrics.horiBearingY / 64)
        else:
            assert char != self._X_HEIGHT_CHAR  # HACK(proste) avoid infinite recursion
            height = self._x_height(normalized_size_26_6)
            bearing = (0, - height)
            shape = (height, int(advance[0]))
            arr = np.zeros(shape, dtype=np.uint8)

        annotation = np.zeros(arr.shape, dtype=np.uint8)
        annotation[:, max(0, int(-bearing[0])):] = 1

        return arr, advance, bearing, annotation

    @lru_cache(maxsize=None)
    def _get_glyph(self, char: str, normalized_size_26_6: int) -> Inscription:
        arr, advance, bearing, annotation = self.__render_glyph(char, normalized_size_26_6)
        ascender, descender, x_height = self._get_metrics(normalized_size_26_6)

        return Inscription(
            arr.copy(),
            InscriptionMetrics(advance, bearing, ascender, descender, x_height),
            annotation.copy(),
            char,
        )


class SlabikarFont(TrueTypeFont):
    def __init__(self, path):
        super().__init__(path)

    def has_char(self, char: str) -> bool:
        if char in ('\u00DF', '\u0141', '\u00F7', '\u00A4', '\u00D7', '\u0026'):
            return False
        return super().has_char(char)

    def _get_glyph_id(
        self, prev_c: Optional[str], curr_c: str, succ_c: Optional[str], size_pt: float, dpi: int
    ) -> Tuple[Hashable, ...]:
        return (
            tuple(self._encode_char(prev_c, curr_c, succ_c)),
            *super()._get_glyph_id(prev_c, curr_c, succ_c, size_pt, dpi),
        )

    @lru_cache(maxsize=None)
    def _get_glyph(
        self, fragments: Tuple[str, ...], curr_c: str, normalized_size_26_6: int
    ) -> Inscription:
        get_glyph = super()._get_glyph
        glyph = Inscription.join(
            get_glyph(fragment, normalized_size_26_6) for fragment in fragments
        )
        return Inscription(glyph.bitmap, glyph.metrics, np.clip(glyph.annotation, 0, 1), curr_c)

    def _encode_char(self, prev_c, c, succ_c):
        if (prev_c is None) or re.match(r'\W', prev_c):
            # add init stroke where needed
            if c in "mnňvwyýzž":
                yield '\u00DF'
            elif c in "beéěfhiíjklprřsštťuúů":
                yield '\u0141'
            elif c == 'x':
                yield '\u00F7'
        elif (not re.match(r'\W', c)):  # add connect stroke where needed
            if prev_c in "BDĎFIÍOÓSŠTŤVWsš":
                yield '\u00A4'
            elif prev_c == 'P':
                yield '\u00D7'

        yield c  # add character itself

        if (succ_c is None) or re.match(r'\W', succ_c):  # add finish stroke where needed
            if c in "AÁCČEÉĚGHJKLMNŇQRŘUÚŮXYÝZŽaábcčdďeéěfghiíjklmnňoópqrřtťuúůvwxyýzž":
                yield '\u0026'


class HandwrittenFont(Font):
    def __init__(self,
        font: Font,
        rotation: Union[float, Tuple[float, float]],
        scale_x: Union[float, Tuple[float, float]],
        scale_y: Union[float, Tuple[float, float]],
        slant: Union[float, Tuple[float, float]],
        weight: Union[float, Tuple[float, float]],
        n_variants: int = None,
        supersampling: int = 4,
        rng: Any = None,
    ):
        super().__init__()
        self._font = font
        self._mu, self._sigma = zip(*(
            self._std_from_range(r) for r in (
                rotation, scale_x, scale_y, slant, weight
            )
        ))
        self._n_variants = n_variants
        self._supersampling = (supersampling or 1)
        self._rng = rng or np.random.default_rng()

        self._glyph_cache = None if (self._n_variants is None) else {}

    def has_char(self, char: str) -> bool:
        return self._font.has_char(char)

    @staticmethod
    def _std_from_range(r):
        try:
            begin, end = r
            return (begin + end) / 2, (end - begin) / 6
        except TypeError:
            return r, 0

    def _sample_distortion(self):
        rotation, scale_x, scale_y, slant, weight = self._rng.normal(
            loc=self._mu, scale=self._sigma
        )
        return weight, Distortions.compose(
            Distortions.slant(slant),
            Distortions.rotate(rotation),
            Distortions.scale(scale_x, scale_y),
        )

    def _get_glyph_id(
        self, prev_c: Optional[str], curr_c: str, succ_c: Optional[str], size_pt: float, dpi: int
    ) -> Tuple[Hashable, ...]:
        return (
            None if (self._n_variants is None) else self._rng.choice(self._n_variants),
            *self._font._get_glyph_id(
                prev_c, curr_c, succ_c, size_pt, self._supersampling * dpi
            ),
        )

    def _get_glyph(self, variant, *super_glyph_id) -> Inscription:
        if self._glyph_cache is None:
            return self.__render_glyph(super_glyph_id)

        key = (variant, *super_glyph_id)
        glyph_ = self._glyph_cache.get(key)
        if glyph_ is None:
            glyph_ = self.__render_glyph(super_glyph_id)
            self._glyph_cache[key] = glyph_

        return glyph_

    # TODO handle aggressive scaling via proper reducing
    def __render_glyph(self, super_glyph_id) -> Inscription:
        glyph = self._font._get_glyph(*super_glyph_id)
        weight, transformation = self._sample_distortion()
        weighted_glyph = weight_inscription(glyph, weight)
        glyph_ = weighted_glyph.transformed(transformation)

        if self._supersampling > 1:
            scale = 1 / self._supersampling
            h, w = (max(1, round(coord * scale)) for coord in glyph_.bitmap.shape)
            bitmap = np.asarray(
                Image.fromarray(glyph_.bitmap).resize((w, h), Image.BILINEAR, reducing_gap=2)
            )
            metrics = glyph_.metrics.transformed(Distortions.scale(scale, scale))
            annotation = np.asarray(
                Image.fromarray(glyph_.annotation).resize((w, h), Image.NEAREST)
            )
            return Inscription(bitmap, metrics, annotation, glyph_.transliteration)

        return glyph_


class Distortions:
    @staticmethod
    def translate(dx, dy):
        return np.array(
            [
                [1, 0, dx],
                [0, 1, dy],
                [0, 0,  1],
            ],
            dtype=np.float32,
        )

    @staticmethod
    def rotate(alpha):
        x = np.deg2rad(alpha)
        sin, cos = np.sin(x), np.cos(x)
        return np.array(
            [
                [cos, - sin, 0],
                [sin,   cos, 0],
                [  0,     0, 1],
            ],
            dtype=np.float32,
        )

    @staticmethod
    def scale(x, y):
        return np.array(
            [
                [x, 0, 0],
                [0, y, 0],
                [0, 0, 1],
            ],
            dtype=np.float32
        )

    @staticmethod
    def shear(x, y):
        return np.array(
            [
                [1, x, 0],
                [y, 1, 0],
                [0, 0, 1],
            ],
            dtype=np.float32,
        )

    @staticmethod
    def compose(*transforms):
        result = None
        for transform in transforms:
            if transform is None:
                continue
            elif result is None:
                result = transform
            else:
                result = np.matmul(transform, result)
        return result

    @classmethod
    def slant(cls, angle):
        shear = np.tan(np.deg2rad(angle))
        scale = 1 / np.sqrt(1 + shear * shear)
        return cls.compose(
            cls.shear(0, - shear),
            cls.scale(scale, scale),
            cls.rotate(angle),
        )


def _spacify(prev, curr, succ, carry, quotes):
    if curr == '-' and (succ == 'li' or (prev == 'e' and succ == 'mail')):
        return False, False

    if curr == ('&', '/', '\\'):
        return False, False

    if curr in ('„', '(', '[', '{'):
        return carry, False

    if curr in ('.', ',', '?', '!', ':', ';', '%', '“', ')', ']', '}'):
        return False, True

    if curr in ('\'', '"'):
        quotes[curr] = quotes.get(curr, 0) + 1
        if quotes[curr] % 2:
            return carry, False
        else:
            return False, True

    return carry, True


def spacify(tokens: Iterable[str]):
    tok_it = iter([*tokens, None])

    prev = None
    curr = None
    succ = next(tok_it)
    carry = False
    quotes = {}

    builder = []
    for token in tok_it:
        prev = curr
        curr = succ
        succ = token

        space, carry = _spacify(prev, curr, succ, carry, quotes)

        if space:
            builder.append(' ')
        builder.append(curr)

    return ''.join(builder)


def weight_inscription(inscription: Inscription, weight: float, filter_size=3) -> Inscription:
    img = Image.fromarray(inscription.bitmap)
    if weight < 0:
        img = Image.blend(img.filter(ImageFilter.MinFilter(size=filter_size)), img, 1 + weight)
    elif weight == 0:
        pass
    else:
        img = Image.blend(img, img.filter(ImageFilter.MaxFilter(size=filter_size)), weight)

    return Inscription(
        bitmap=np.asarray(img),
        metrics=inscription.metrics,
        annotation=inscription.annotation,
        transliteration=inscription.transliteration,
    )


def collate_inscriptions(
    top: Inscription,
    mid: Inscription,
    dwn: Inscription,
    leading: int,
    max_width: int = None,
    rng: Any = None,
) -> Inscription:
    rng = np.random.default_rng(rng)

    top_ascender = - (top or mid).metrics.bearing[1]
    top_leading = leading * bool(top)
    dwn_leading = leading * bool(dwn)
    dwn_descender = (dwn or mid).bitmap.shape[0] + (dwn or mid).metrics.bearing[1]

    baselines = np.cumsum([top_ascender, top_leading, dwn_leading])
    lines = (top, mid, dwn)

    height = max(
        (round(baseline + line.metrics.bearing[1]) + line.bitmap.shape[0])
        for baseline, line in zip(baselines, lines) if line
    )
    width = max(line.bitmap.shape[1] for line in lines if line)
    if max_width is not None:
        assert max_width >= width
        width = rng.integers(width, max_width, endpoint=True)
    canvas = np.zeros((height, width), dtype=np.uint8)
    annotation_canvas = np.zeros_like(canvas)

    metrics = None
    for baseline, line in zip(baselines, lines):
        if line is None:
            continue

        h, w = line.bitmap.shape
        gap = width - w

        x = rng.integers(gap, endpoint=True)
        canvas_slice = slice(x, (width - gap + x))

        y = round(baseline + line.metrics.bearing[1])
        canvas_crop = canvas[y:(y + h), canvas_slice]
        canvas_crop[:] = np.maximum(canvas_crop, line.bitmap)

        if line == mid:
            annotation_canvas[y:(y + h), canvas_slice] = mid.annotation
            metrics = mid.metrics.transformed(Distortions.translate(x, y))

    return Inscription(canvas, metrics, annotation_canvas, mid.transliteration)
