from typing import Iterable, Iterator, Optional, Set, Tuple

import bs_decoder
import kenlm
import numpy as np
import tensorflow as tf

from hawran import dataset, storage
from hawran.charset import Charset
from hawran.utils import logprobs_from_logits

SPACE_SUB = '~'


class LanguageModel:
    def __init__(self, kenlm_path: str):
        self._model = kenlm.Model(kenlm_path)

    @property
    def empty_state(self) -> kenlm.State:
        state = kenlm.State()
        self._model.NullContextWrite(state)
        return state

    def __call__(self, prev_state, token):
        out_state = kenlm.State()
        ret = self._model.BaseFullScore(
            prev_state, self._preprocess_token(token), out_state
        )
        return ret.log_prob, out_state, ret.oov

    def prune_phrase(self, phrase: str, chain_len: Optional[int]):
        chain_len = chain_len or self._model.order

        prev_state = kenlm.State()
        out_state = kenlm.State()
        self._model.NullContextWrite(out_state)

        token_data = list(self.token_iterator(phrase))

        if not token_data:
            return

        token_begins, token_ends, tokens = zip(*self.token_iterator(phrase))

        ngram_sizes = []
        oovs = []
        for token in tokens:
            prev_state, out_state = out_state, prev_state
            ret = self._model.BaseFullScore(
                prev_state, self._preprocess_token(token), out_state
            )
            ngram_sizes.append(ret.ngram_length)
            oovs.append(ret.oov)
        ngram_sizes = np.array(ngram_sizes)
        oovs = np.array(oovs)

        mask = np.r_[False, ngram_sizes >= chain_len, False]

        begins = np.flatnonzero(mask[1:-1] & ~mask[:-2])
        ends = np.flatnonzero(mask[1:-1] & ~mask[2:])

        begins += 1 - ngram_sizes[begins]
        begins += oovs[begins]

        assert (begins <= (ends + 1 - chain_len)).all()
        assert (begins >= 0).all()

        for begin, end in zip(begins, ends):
            interval_begin = token_begins[begin]
            interval_end = token_ends[end]
            yield interval_begin, interval_end, phrase[interval_begin:interval_end]

    def token_iterator(self, phrase: str) -> Iterator[Tuple[int, int, str]]:
        raise NotImplementedError

    def _preprocess_token(self, token: str) -> str:
        return token


class WordLanguageModel(LanguageModel):
    def __init__(self, kenlm_path: str, word_delimiters: Iterable[str]):
        super().__init__(kenlm_path)
        self._word_delimiters = frozenset(word_delimiters)

    def token_iterator(self, phrase: str) -> Iterator[Tuple[int, int, str]]:
        if not phrase:
            return
        begin = 0
        for end, char in enumerate(phrase):
            if char in self._word_delimiters:
                if end > begin:
                    yield (begin, end, phrase[begin:end])
                if char != ' ':
                    yield (end, end + 1, phrase[end:(end + 1)])
                begin = end + 1
        end += 1
        if end > begin:
            yield (begin, end, phrase[begin:end])


class CharLanguageModel(LanguageModel):
    def __init__(self, kenlm_path: str, space_sub: str = None):
        super().__init__(kenlm_path)
        self._space_sub = space_sub or SPACE_SUB

    def __call__(self, prev_state, token):
        return super().__call__(
            prev_state, self._space_sub if (token == ' ') else token
        )

    def token_iterator(self, phrase: str) -> Iterator[Tuple[int, int, str]]:
        yield from (
            (char_idx, char_idx + 1, char) for char_idx, char in enumerate(phrase)
        )

    def _preprocess_token(self, token: str) -> str:
        return self._space_sub if (token == ' ') else token


class Decoder:
    def decode(self, logits, **kwargs):
        raise NotImplementedError

    def predict_ds(self, ds: dataset.Dataset):
        return DecodingsDataset(ds, self)


class DecodingsDataset(dataset.TransformedDataset):
    def __init__(self, predictions_ds, decoder):
        super().__init__(source=predictions_ds)
        self._decoder = decoder

    def _transform(self, logits, **components):
        decodings = self._decoder.decode(logits)
        labellings, scores = zip(*decodings)

        label_widths = np.array(list(map(len, labellings)), dtype=np.int32)
        labels = np.zeros((len(decodings), label_widths.max()), dtype=np.uint8)
        for l_i, labelling in enumerate(labellings):
            labels[l_i, :len(labelling)] = labelling
        scores = np.array(scores, dtype=np.float32)

        yield {
            **components,
            'logits': logits,
            'labels': labels,
            'label_widths': label_widths,
            'scores': scores,
        }


class GreedyDecoder(Decoder):
    def __init__(self, blank_idx: int):
        super().__init__()
        self._blank_idx = blank_idx

    def decode(self, logits):
        logprobs = logprobs_from_logits(logits)
        labels = logprobs.argmax(axis=-1)
        diff_mask = np.r_[True, labels[1:] != labels[:-1]]
        fg_mask = labels != self._blank_idx
        decoding = labels[diff_mask & fg_mask]
        logprob = logprobs.max(axis=-1).sum()

        return [(decoding, logprob)]


class RawBSDecoder(Decoder):
    def __init__(self, beam_width: int):
        super().__init__()
        self._beam_width = beam_width

    def decode(self, logits, beam_width: int = None):
        beam_width = beam_width or self._beam_width
        decodings, logprobs = tf.nn.ctc_beam_search_decoder(
            tf.roll(logits[:, None, :], -1, 2),
            [logits.shape[0]],
            beam_width,
            beam_width
        )
        return [
            (tf.sparse.to_dense(decoding).numpy()[0] + 1, logprob)
            for decoding, logprob in zip(decodings, logprobs.numpy()[0])
        ]


class KenLMBSDecoder(Decoder):
    def __init__(
        self,
        beam_width: int,
        pruning: int,
        char_lm: str,
        word_lm: str,
        charset: Charset,
        alpha: float = 1.2,
        char_beta: float = 0,
        word_beta: float = 5.5,
        gamma: float = 1.0,
        space_sub: str = None,
        use_cscore: bool = False,
    ):
        self._beam_width = beam_width
        self._pruning = pruning
        self._gamma = gamma
        self._charset = charset
        self._use_cscore = use_cscore

        chars = self._charset.out_chars.tolist()
        chars.insert(self._charset.blank_idx, '\0')
        word_delimiter_idxs, _ = self._charset.subset(
            self._charset.ALPHANUM_RE, complement=True
        )

        with storage.open(char_lm, 'rb') as char_f, \
             storage.open(word_lm, 'rb') as word_f:
            self._ray_scorer = bs_decoder.RayScorer(
                chars,
                self._charset.blank_idx,
                set(word_delimiter_idxs),
                space_sub or SPACE_SUB,
                char_f.name,
                word_f.name,
                alpha,
                char_beta,
                word_beta,
            )

    def decode(
        self,
        logits,
        beam_width: int = None,
        pruning: int = None,
        gamma: int = None,
        use_cscore: bool = None,
    ):
        logprobs = logprobs_from_logits(logits)
        return bs_decoder.decode(
            logprobs,
            self._charset.blank_idx,
            beam_width or self._beam_width,
            pruning or self._pruning,
            self._ray_scorer,
            gamma or self._gamma,
            self._use_cscore if (use_cscore is None) else use_cscore,
        )


def expected_extended_labels(logprobs, labels, blank_idx):
    labels = np.asarray(labels)
    labels_repetitive = np.r_[False, labels[:-1] == labels[1:]]

    n_labels = len(labels)
    n_timesteps = len(logprobs)

    # action indices
    LABEL, BLANK = range(2)
    # path head indice (those ending with blank underscored)
    PREV, PREV_, CURR, CURR_ = range(4)

    # dynamic programming tables initialization
    actions = np.empty((2, n_timesteps, 1 + n_labels), dtype=np.uint8)
    prefix_logprobs = np.empty((4, 1 + n_labels), dtype=np.float32)
    prefix_logprobs[:] = - np.inf
    prefix_logprobs[[CURR_, PREV_], 0] = 0

    # forward pass - action selection
    for x, label_logprobs in enumerate(logprobs[:, labels]):
        blank_logprob = logprobs[x, blank_idx]

        prefix_logprobs[[PREV, PREV_], :] = prefix_logprobs[[CURR, CURR_], :]

        path_logprobs = np.array([
            # repeat label
            label_logprobs + prefix_logprobs[PREV, 1:],
            # label after blank
            label_logprobs + prefix_logprobs[PREV_, :-1],
            # label after differing label
            np.where(
                labels_repetitive,
                (- np.inf),
                (label_logprobs + prefix_logprobs[PREV, :-1]),
            ),
        ])
        actions[LABEL, x, 1:] = np.argmax(path_logprobs, axis=0)
        prefix_logprobs[CURR, 1:] = np.max(path_logprobs, axis=0)

        path_logprobs_ = np.array([
            # repeat blank
            blank_logprob + prefix_logprobs[PREV_, :],
            # blank after label
            blank_logprob + prefix_logprobs[PREV, :],
        ])
        actions[BLANK, x, :] = np.argmax(path_logprobs_, axis=0)
        prefix_logprobs[CURR_, :] = np.max(path_logprobs_, axis=0)

    # backward pass - path reconstruction
    y = len(labels) - 1
    blank_end = prefix_logprobs[CURR_, -1] > prefix_logprobs[CURR, -1]
    extended_labels = [blank_idx if blank_end else labels[y]]
    step_logprobs = [logprobs[-1, extended_labels[-1]]]

    for x in range(1, actions.shape[1])[::-1]:
        if extended_labels[-1] == blank_idx:
            action = actions[BLANK, x, 1 + y]
            if action == 0:  # repeat blank
                extended_labels.append(blank_idx)
            else:  # action == 1:  # label in front of blank
                extended_labels.append(labels[y])
        else:
            action = actions[LABEL, x, 1 + y]
            if action == 0:  # repeat label
                extended_labels.append(labels[y])
            elif action == 1:  # blank in front of label
                y -= 1
                extended_labels.append(blank_idx)
            else:  # action == 2:  # label in front of differing label
                y -= 1
                extended_labels.append(labels[y])
        step_logprobs.append(logprobs[x - 1, extended_labels[-1]])

    extended_labels = np.array(extended_labels[::-1])
    logprob = np.array(step_logprobs[::-1])

    diff_mask = (extended_labels[:-1] != extended_labels[1:])
    label_mask = (extended_labels != blank_idx)
    falling_edges = np.flatnonzero(diff_mask & label_mask[:-1])
    raising_edges = np.flatnonzero(diff_mask & label_mask[1:]) + 1

    gap_count = n_labels - 1
    gap_begins = falling_edges[:gap_count]
    gap_ends = raising_edges[(len(raising_edges) - gap_count):]

    boundaries = np.r_[
        0, (gap_begins + gap_ends) / 2 + 0.5, len(extended_labels)
    ]

    return extended_labels, logprob, boundaries
