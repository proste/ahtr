import re
import unicodedata
from typing import Callable, Iterable, List

import numpy as np


class Charset:
    ALPHANUM_RE = '[LN].'

    def __init__(
        self, charset, transcription_table=None, blank_idx=0, unk_idx=0,
    ):
        assert unicodedata.normalize('NFC', charset) == charset

        if transcription_table:
            charset_ = set(charset)
            assert all(
                (unicodedata.normalize('NFC', transcription) == transcription)
                for transcription in transcription_table.values()
            )
            assert all(
                (set(transcription) <= charset_)
                for transcription in transcription_table.values()
            )
        else:
            transcription_table = {c: c for c in charset}

        unk_idx = unk_idx % len(charset)
        if blank_idx <= unk_idx:
            unk_idx += 1

        charset = list(charset)
        charset.insert(blank_idx, '')

        self._charset = np.array(charset)
        self._char_to_idx = {
            char: char_idx for char_idx, char in enumerate(charset)
        }
        self._trans_table = {
            symbol: (
                transcription,
                tuple(self._char_to_idx[char] for char in transcription)
            )
            for symbol, transcription in transcription_table.items()
        }
        self._blank_idx = blank_idx
        self._unk_idx = unk_idx

    def __len__(self):
        return len(self._charset) - 1

    @property
    def blank_idx(self):
        return self._blank_idx

    @property
    def in_chars(self):
        return np.asarray(list(self._trans_table.keys()))

    @property
    def out_chars(self):
        return np.r_[self._charset[:self._blank_idx], self._charset[self._blank_idx + 1:]]

    def subset(self, category_re: 'str', complement: bool = False):
        category_re = re.compile(category_re)
        idxs_chars = [
            (char_idx, char)
            for char_idx, char in enumerate(self._charset.tolist())
            if char and (
                complement
                != bool(category_re.fullmatch(unicodedata.category(char)))
            )
        ]

        if idxs_chars:
            idxs, chars = zip(*idxs_chars)
            return idxs, chars

        return None, None

    def transcribe(self, text):
        return [
            (
                symbol,
                *self._trans_table.get(
                    symbol,
                    (self._charset[self._unk_idx], (self._unk_idx,)),
                )
            )
            for symbol in unicodedata.normalize('NFC', text)
        ]

    def chars_to_indices(self, chars):
        return np.vectorize(lambda c: self._char_to_idx[c], otypes=(np.uint8,))(chars)

    def indices_to_chars(self, indices):
        return self._charset[np.asarray(indices, dtype=np.int32)]

    def indices_to_str(self, indices):
        chars = self.indices_to_chars(indices)
        assert chars.ndim == 1
        return ''.join(chars)

    def join_chars(self, chars, axis=-1):
        return np.apply_along_axis(
            lambda s: np.array(''.join(s), dtype=np.object),
            axis,
            chars
        ).astype(np.unicode)

    def retranscribe(
        self,
        text: str,
        subset: Iterable[str] = None,
        has_char: Callable[[str], bool] = None,
    ):
        if has_char is None:
            assert subset is not None
            subset = frozenset(subset)
            def has_char(char):
                return char in subset

        def retranscribe_(char, transcription):
            result = []
            for alternative in (char, transcription):
                for c in alternative:
                    if has_char(c):
                        result.append(c)
                        continue
                    normalized_char = unicodedata.normalize('NFD', char)
                    if (len(normalized_char) > 1) and has_char(normalized_char[0]):
                        result.append(normalized_char[0])
                        continue
                    break
                else:
                    return result
                result = []
            return result

        return ''.join(
            char_
            for char, transcription, _ in self.transcribe(text)
            for char_ in retranscribe_(char, transcription)
        )


syn_v4_charset = Charset(
    charset=(
        '�AaÁáBbCcČčDdĎďEeÉéĚěFfGgHhIiÍíJjKkLlMmNnŇňOoÓóPpQqRrŘřSs'
        'ŠšTtŤťUuÚúŮůVvWwXxYyÝýZzŽž0123456789 .,?!:;"\'()+-*/<>=&|%'
    ),
    transcription_table={
        'ū': 'u', 'û': 'u', 'u': 'u', 'ù': 'u', 'ü': 'u', 'ų': 'u', 'ű': 'u', 'у': 'u', 'ὐ': 'u',
        'υ': 'u', 'ὺ': 'u', 'í': 'í', 'ě': 'ě', 'T': 'T', 'Ţ': 'T', 'Т': 'T', 'Τ': 'T', 'Ц': 'T',
        'į': 'i', 'i': 'i', 'ï': 'i', 'î': 'i', 'ī': 'i', 'ì': 'i', 'ỉ': 'i', 'и': 'i', 'ἴ': 'i',
        'й': 'i', 'ὶ': 'i', 'ι': 'i', 'ı': 'i', 'е': 'i', 'і': 'i', 'h': 'h', 'х': 'h', 'ъ': 'h',
        'ħ': 'h', 'Ý': 'Ý', 'È': 'E', 'Ẹ': 'E', 'Ê': 'E', 'Ë': 'E', 'Ẽ': 'E', 'E': 'E', 'Ề': 'E',
        'Ȩ': 'E', 'Ē': 'E', 'Ę': 'E', 'Ė': 'E', 'Ή': 'E', 'Η': 'E', 'Ε': 'E', 'Ἐ': 'E', 'Ὲ': 'E',
        'Έ': 'E', 'ť': 'ť', 'Ù': 'U', 'Ű': 'U', 'U': 'U', 'Ü': 'U', 'Ū': 'U', 'Ų': 'U', 'Û': 'U',
        'Υ': 'U', 'Ὺ': 'U', 'У': 'U', 'ř': 'ř', 'â': 'a', 'à': 'a', 'ā': 'a', 'ä': 'a', 'a': 'a',
        'ą': 'a', 'ả': 'a', 'ã': 'a', 'ǻ': 'a', 'ă': 'a', 'ǎ': 'a', 'å': 'a', 'ἀ': 'a', 'ά': 'a',
        'α': 'a', 'а': 'a', 'ἁ': 'a', 'ἄ': 'a', 'Ÿ': 'Y', 'Ŷ': 'Y', 'Y': 'Y', 'Ы': 'Y', 'Ю': 'Y',
        'Ї': 'Y', 'Я': 'Y', 'Ą': 'A', 'Ā': 'A', 'Ǎ': 'A', 'Ä': 'A', 'Â': 'A', 'À': 'A', 'A': 'A',
        'Å': 'A', 'Ả': 'A', 'Ã': 'A', 'Ă': 'A', 'Ǻ': 'A', 'А': 'A', 'Ἄ': 'A', 'Ἁ': 'A', 'Α': 'A',
        'Ά': 'A', 'Ἀ': 'A', 'ć': 'c', 'ç': 'c', 'c': 'c', 'ч': 'c', 'Ž': 'Ž', 'S': 'S', 'Ş': 'S',
        'Ś': 'S', 'Ҫ': 'S', 'Ш': 'S', 'Σ': 'S', 'С': 'S', 'Ь': 'S', 'ğ': 'g', 'ģ': 'g', 'ġ': 'g',
        'g': 'g', 'γ': 'g', 'Ġ': 'G', 'Ğ': 'G', 'Ģ': 'G', 'G': 'G', 'Γ': 'G', 'Ó': 'Ó', 'ď': 'ď',
        'n': 'n', 'ń': 'n', 'ņ': 'n', 'ñ': 'n', 'н': 'n', 'ν': 'n', 'ò': 'o', 'õ': 'o', 'ö': 'o',
        'ō': 'o', 'ô': 'o', 'ő': 'o', 'ǫ': 'o', 'o': 'o', 'ό': 'o', 'ὄ': 'o', 'о': 'o', 'ο': 'o',
        'ὤ': 'o', 'ω': 'o', 'ὦ': 'o', 'ὸ': 'o', 'ῶ': 'o', 'ø': 'o', 'š': 'š', 'R': 'R', 'Ŕ': 'R',
        'Р': 'R', 'Ķ': 'K', 'K': 'K', 'К': 'K', 'Κ': 'K', 'Ď': 'Ď', 'Ì': 'I', 'Î': 'I', 'Ī': 'I',
        'Ï': 'I', 'Ỉ': 'I', 'I': 'I', 'Į': 'I', 'İ': 'I', 'Ι': 'I', 'Ὶ': 'I', 'И': 'I', 'І': 'I',
        'Й': 'I', 'Ἴ': 'I', 'Е': 'I', 'Š': 'Š', 'q': 'q', 'Ľ': 'L', 'Ĺ': 'L', 'Ļ': 'L', 'L': 'L',
        'Ł': 'L', 'Ɫ': 'L', 'Л': 'L', 'Ŀ': 'L', 'Λ': 'L', 't': 't', 'ţ': 't', 'τ': 't', 'ц': 't',
        'т': 't', 'O': 'O', 'Õ': 'O', 'Ō': 'O', 'Ő': 'O', 'Ò': 'O', 'Ô': 'O', 'Ö': 'O', 'Ǫ': 'O',
        'Ο': 'O', 'Ό': 'O', 'Ø': 'O', 'О': 'O', 'Ω': 'O', 'Ὄ': 'O', 'Ὦ': 'O', 'Ὤ': 'O', 'Ὸ': 'O',
        'ú': 'ú', 'Ú': 'Ú', 'N': 'N', 'Ñ': 'N', 'Ņ': 'N', 'Ń': 'N', 'Н': 'N', 'Ν': 'N', 'C': 'C',
        'Ç': 'C', 'Ć': 'C', 'Ч': 'C', 's': 's', 'ş': 's', 'ś': 's', 'ҫ': 's', 'ß': 's', 'с': 's',
        'ь': 's', 'ш': 's', 'σ': 's', 'ſ': 's', 'B': 'B', 'Β': 'B', 'Б': 'B', 'ẽ': 'e', 'ė': 'e',
        'ę': 'e', 'ê': 'e', 'ề': 'e', 'ȩ': 'e', 'è': 'e', 'ē': 'e', 'e': 'e', 'ẹ': 'e', 'ë': 'e',
        'η': 'e', 'ε': 'e', 'ἐ': 'e', 'έ': 'e', 'ὲ': 'e', 'ή': 'e', 'F': 'F', 'Ƒ': 'F', 'ň': 'ň',
        'ľ': 'l', 'ĺ': 'l', 'ļ': 'l', 'l': 'l', 'λ': 'l', 'ł': 'l', 'ɫ': 'l', 'л': 'l', 'ŀ': 'l',
        'Q': 'Q', 'm': 'm', 'μ': 'm', 'м': 'm', 'ó': 'ó', 'x': 'x', 'ξ': 'x', 'ý': 'ý', 'ÿ': 'y',
        'ŷ': 'y', 'y': 'y', 'ї': 'y', 'ю': 'y', 'ы': 'y', 'я': 'y', 'k': 'k', 'ķ': 'k', 'к': 'k',
        'κ': 'k', 'Ż': 'Z', 'Ź': 'Z', 'Z': 'Z', 'Ζ': 'Z', 'З': 'Z', 'Ж': 'Z', 'á': 'á', 'ž': 'ž',
        'Ť': 'Ť', 'ź': 'z', 'ż': 'z', 'z': 'z', 'з': 'z', 'ж': 'z', 'ζ': 'z', 'p': 'p', 'п': 'p',
        'π': 'p', 'ŕ': 'r', 'r': 'r', 'р': 'r', 'H': 'H', 'Х': 'H', 'Ъ': 'H', 'Ħ': 'H', 'Í': 'Í',
        'Ř': 'Ř', 'É': 'É', 'v': 'v', 'в': 'v', 'Č': 'Č', 'f': 'f', 'ƒ': 'f', 'ς': 'f', 'W': 'W',
        'b': 'b', 'б': 'b', 'β': 'b', 'Ň': 'Ň', 'V': 'V', 'В': 'V', 'P': 'P', 'П': 'P', 'Π': 'P',
        'd': 'd', 'đ': 'd', 'д': 'd', 'δ': 'd', 'č': 'č', 'M': 'M', 'Μ': 'M', 'М': 'M', 'w': 'w',
        'X': 'X', 'Ξ': 'X', 'D': 'D', 'Д': 'D', 'Đ': 'D', 'Δ': 'D', 'J': 'J', 'Ј': 'J', 'j': 'j',
        'ј': 'j', 'ů': 'ů', 'Á': 'Á', 'é': 'é', 'Ě': 'Ě', 'Ů': 'Ů', 'ρ': 'rh', 'Χ': 'Ch',
        'æ': 'ae', 'Ρ': 'Rh', 'Ψ': 'Ps', 'ψ': 'ps', 'Æ': 'Ae', 'Φ': 'Ph', 'Θ': 'Th', 'Þ': 'Th',
        'ﬁ': 'fi', 'φ': 'ph', 'þ': 'th', 'θ': 'th', 'œ': 'oe', 'χ': 'ch', 'Œ': 'Oe', '0': '0',
        '1': '1', '2': '2', '3': '3', '4': '4', '5': '5', '6': '6', '7': '7', '8': '8', '9': '9',
        '.': '.', ',': ',', '-': '-', '–': '-', '—': '-', '¯': '-', '_': '-', '―': '-', '‒': '-',
        '−': '-', '•': '-', '·': '-', '̶': '-', ':': ':', ')': ')', ']': ')', '}': ')', '(': '(',
        '[': '(', '{': '(', '>': '>', '›': '>', '<': '<', '‹': '<', '«': '>>', '»': '<<', '"': '"',
        '„': '"', '“': '"', '”': '"', '‟': '"', '˝': '"', '?': '?', '!': '!', '/': '/', '\\': '/',
        '∕': '/', ' ': ' ', '*': '*', ';': ';', '%': '%', "'": "'", '‘': "'", '’': "'", '‚': "'",
        '`': "'", '´': "'", '‛': "'", '…': '...', '+': '+', '&': '&', '×': 'x', '=': '=', '|': '|',
        '¦': '|',
    },
    blank_idx=0
)


beta_charset = Charset(
    charset=(
        "abcdefghijklmnopqrstuvwxyzáéíóúýčďěňřšťůžABCDEFGHIJKLM"
        "NOPQRSTUVWXYZÁÉÍÓÚÝČĎĚŇŘŠŤŮŽ0123456789!%'(),-./:;?§“„� "
    ),
    transcription_table=None,
    blank_idx=0,
    unk_idx=-2,
)


# NOTE(proste): convenience tools to derive new charsets

class Char:
    def __init__(self, char: str, count: int = 0, **kwargs):
        self.char = unicodedata.normalize('NFC', char)[0]
        self._decomposed = unicodedata.normalize('NFD', char)
        self.count = count
        self.decomposed = self._decomposed.encode('utf-8')
        self.name = unicodedata.name(self.char, 'NONAME')

    def __hash__(self) -> int:
        return ord(self.char)

    def __eq__(self, other) -> bool:
        return isinstance(other, self.__class__) and (hash(self) == hash(other))

    def __str__(self) -> str:
        return self.char

    def __repr__(self) -> str:
        return f'Char(char={self.char!r}, count={self.count}, decomposed={self.decomposed}, name={self.name!r})'


def compacted_charset(charset: List[Char]):
    CZECH_ACCENTS = {
        Char(char) for char_ in 'áéíóúůýěžščřďťň' for char in (char_, char_.upper())
    }

    def is_alpha(char):
        return (b'a' <= char.decomposed[0:1] <= b'z') or (b'A' <= char.decomposed[0:1] <= b'Z')

    # NOTE(proste): handle standard latin letters
    alphas = {}
    for char in charset:
        if is_alpha(char):
            if char in CZECH_ACCENTS:
                alphas[char.char] = [char]
            else:
                alphas.setdefault(char.decomposed[0:1].decode('utf_8'), []).append(char)

    # NOTE(proste): handle non-standard of off-latin letters
    name_re = re.compile('^([A-Z]*) (SMALL|CAPITAL) (LETTER|LIGATURE) (.*)')
    for char in (
        char for char in charset
        if (not any(char in alphas_ for alphas_ in alphas.values()))
    ):
        match = name_re.match(char.name)
        if match:
            origin, capitalization, _, letter = match.groups()
        else:
            continue

        if origin == 'LATIN':
            letter = min(letter.split(' '), key=len)[:2]
        elif origin == 'GREEK':
            letter, *_ = letter.split(' ', maxsplit=1)
            if letter[1] in ('H', 'S'):
                letter = letter[:2]
            else:
                letter = letter[0]
        elif origin == 'CYRILLIC':
            letter = min(letter.split(' '), key=len)
            letter = letter.strip('EA') or letter
            if len(letter) > 1:
                letter = letter[0]
        else:
            continue

        if capitalization == 'SMALL':
            letter = letter.lower()
        else:
            letter = letter.capitalize()

        alphas.setdefault(letter, []).append(char)

    # NOTE(proste): handle digits
    digits = {
        char.char: [char]
        for char in sorted(charset, key=lambda c: c.decomposed)
        if b'0' <= char.decomposed <= b'9'
    }

    # NOTE(proste): group some popular and useful symbols
    symbols = {
        '.': [Char(char='.')],
        ',': [Char(char=',')],
        '-': [
            Char(char='-'),
            Char(char='–'),
            Char(char='—'),
            Char(char='¯'),
            Char(char='_'),
            Char(char='―'),
            Char(char='‒'),
            Char(char='−'),
            Char(char='•'),
            Char(char='·'),
            Char(char='̶'),
        ],
        ':': [Char(char=':')],
        ')': [
            Char(char=')'),
            Char(char=']'),
            Char(char='}'),
        ],
        '(': [
            Char(char='('),
            Char(char='['),
            Char(char='{'),
        ],
        '>': [
            Char(char='>'),
            Char(char='›'),
        ],
        '<': [
            Char(char='<'),
            Char(char='‹'),
        ],
        '>>': [Char(char='«')],
        '<<': [Char(char='»')],
        '"': [
            Char(char='"'),
            Char(char='„'),
            Char(char='“'),
            Char(char='”'),
            Char(char='‟'),
            Char(char='˝'),
        ],
        '?': [Char(char='?')],
        '!': [Char(char='!')],
        '/': [
            Char(char='/'),
            Char(char='\\'),
            Char(char='∕'),
        ],
        ' ': [Char(char=' ')],
        '*': [Char(char='*')],
        ';': [Char(char=';')],
        '%': [Char(char='%')],
        "'": [
            Char(char="'"),
            Char(char='‘'),
            Char(char='’'),
            Char(char='‚'),
            Char(char='`'),
            Char(char='´'),
            Char(char='‛'),
        ],
        '...': [Char(char='…')],
        '+': [Char(char='+')],
        '&': [Char(char='&')],
        'x': [Char(char='×')],
        '=': [Char(char='=')],
        '|': [
            Char(char='|'),
            Char(char='¦'),
        ],
    }

    table = {
        str(char): letter
        for kind in (alphas, digits, symbols)
        for letter, group in kind.items()
        for char in group
    }

    charset = ''.join(sorted(set(''.join(table.values()))))

    return charset, table
