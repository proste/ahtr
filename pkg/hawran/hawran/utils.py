import itertools
from typing import Any, Callable, Iterable, Iterator, Tuple

import numpy as np
import scipy.special


def groupby_sorted(
    iterable: Iterable[Any], key: Callable[[Any], Any] = None, value: Callable[[Any], Any] = None
) -> Iterator[Tuple[Any, Iterator[Any]]]:
    """
    Convenience function for grouping of randomly ordered iterables.

    `key` and `value` parameters help to solve cases where
        - one is grouping complex structures (key is not the whole element)
        - one is grouping by external, e.g. zipped, keys (value is not the whole element)

    Parameters
    ----------
    iterable
        collection to be grouped
    key
        function mapping elements of `iterable` to keys used for grouping;
        optional - defaults to identity
    value
        function mapping elements of `iterable` to values (elements of resulting groups);
        optional - defaults to identity

    Yields
    ------
    group_key
        the key of the `group`
    group
        iterater of values (`value(element)`)
    """
    return (
        (k, group if (value is None) else (value(v) for v in group))
        for k, group in groupby(sorted(iterable, key=key), key=key)
    )


def groupby(iterable, key=None, val=None, key_val=None, sort=False):
    def key_(kv):
        return kv[0]

    def val_(kv):
        return kv[1]

    def identity(arg):
        return arg

    if key_val is not None:
        assert (key is None) and (val is None)

        key = key_
        val = val_
        iterable = map(key_val, iterable)
    else:
        if key is None:
            key = identity
        if val is None:
            val = identity

    if sort:
        iterable = sorted(iterable, key=key)

    for group_key, group in itertools.groupby(iterable, key=key):
        yield group_key, (val(item) for item in group)


def sliding_window(iterable: Iterable[Any], size: int) -> Tuple[Any]:
    it = iter(iterable)
    window = [next(it) for _ in range(size - 1)]
    for elem in it:
        window.append(elem)
        window = window[-size:]
        yield tuple(window)


def logprobs_from_logits(logits, axis=-1):
    return logits - np.expand_dims(
        scipy.special.logsumexp(logits, axis=axis), axis=axis
    )
