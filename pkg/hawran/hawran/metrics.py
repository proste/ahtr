from typing import Any, NamedTuple, Optional, Tuple

import Levenshtein


class Metric:
    class Result(NamedTuple):
        value: float
        weight: float

    def __init__(self, name: str = None):
        self.name = name or self.__class__.__name__

    def __call__(self, true: Any, pred: Any) -> Optional['Metric.Result']:
        raise NotImplementedError


class EditDistance(Metric):
    def __call__(self, true: str, pred: str) -> Metric.Result:
        return self.Result(Levenshtein.distance(true, pred) / len(true), len(true))


class ClipEditDistance(EditDistance):
    def __call__(self, true: str, pred: str) -> Optional[Metric.Result]:
        true_ = self._clip_phrase(true)

        if not true_:
            return None

        return min(
            (
                super(self.__class__, self).__call__(
                    true_, pred[begin:(begin + len(true_))]
                )
                for begin in range(max(0, len(pred) - len(true_)) + 1)
            ),
            key=(lambda r: r.value)
        )

    @staticmethod
    def _clip_phrase(phrase: str) -> str:
        words = phrase.split(' ')
        return ' '.join(words[1:-1])


class AggregatedMetric:
    def __init__(self, metric: Metric, name: str = None):
        self._metric = metric
        self._name = name or 'Aggregated'

    def add(self, true: Any, pred: Any):
        raise NotImplementedError

    @property
    def name(self) -> str:
        return self._name + self._metric.name

    @property
    def stats(self) -> dict:
        raise NotImplementedError

    @property
    def value(self) -> float:
        raise NotImplementedError


class MicroMetric(AggregatedMetric):
    def __init__(self, metric: Metric, name: str = None):
        super().__init__(metric, name or 'Micro')
        self._acc = 0
        self._weight = 0

    def add(self, true: str, pred: str):
        result = self._metric(true, pred)

        if result is None:
            return

        self._acc += result.value * result.weight
        self._weight += result.weight

    @property
    def stats(self) -> dict:
        return {
            'acc': self._acc,
            'weight': self._weight,
        }

    @property
    def value(self) -> float:
        if self._weight == 0:
            return float('nan')
        return self._acc / self._weight


class MacroMetric(AggregatedMetric):
    def __init__(self, metric: Metric, name: str = None):
        super().__init__(metric, name or 'Macro')
        self._acc = 0
        self._n_samples = 0

    def add(self, true: str, pred: str):
        result = self._metric(true, pred)

        if result is None:
            return

        self._acc += result.value
        self._n_samples += 1

    @property
    def stats(self) -> dict:
        return {
            'acc': self._acc,
            'n_samples': self._n_samples,
        }

    @property
    def value(self) -> float:
        if self._n_samples == 0:
            return float('nan')
        return self._acc / self._n_samples

