import logging
import os
import traceback
from typing import Union

import tensorflow as tf


logger = logging.getLogger(__name__)


class CustomMetric(tf.keras.metrics.Metric):
    def __init__(self, name):
        super().__init__(name=name)
        # NOTE(proste): bit of a hack to be able to get training state on epoch end during fit
        self._last_states = None

    def reset_states(self):
        self._last_states = [var.numpy() for var in self.variables]

        for var in self.variables:
            var.assign(tf.zeros_like(var))

    def _rollback_states(self):
        if self._last_states is None:
            raise RuntimeError(f'Metric {self.name} has no state to rollback to.')

        for var, value in zip(self.variables, self._last_states):
            var.assign(value)
        self._last_states = None

    def log_custom_summaries(self):
        pass


class CustomMetricsLogger(tf.keras.callbacks.Callback):
    def __init__(self, logdir: Union[str, tf.keras.callbacks.TensorBoard], *metrics: CustomMetric):
        super().__init__()
        self._logdir = logdir
        self._train_writer_ = None
        self._val_writer_ = None
        self._metrics = metrics
        self._was_validated = None

    @property
    def _train_writer(self):
        if self._train_writer_ is None:
            if isinstance(self._logdir, tf.keras.callbacks.TensorBoard):
                return self._logdir._train_writer
            else:
                self._train_writer_ = tf.summary.create_file_writer(
                    os.path.join(self._logdir, 'train')
                )
        return self._train_writer_

    @property
    def _val_writer(self):
        if self._val_writer_ is None:
            if isinstance(self._logdir, tf.keras.callbacks.TensorBoard):
                return self._logdir._val_writer
            else:
                self._val_writer_ = tf.summary.create_file_writer(
                    os.path.join(self._logdir, 'validate')
                )
        return self._val_writer_

    def on_epoch_begin(self, epoch, logs=None):
        self._was_validated = False

    def on_test_end(self, logs=None):
        self._was_validated = True

    def on_epoch_end(self, epoch, logs=None):
        if self._was_validated:
            with self._val_writer.as_default(step=epoch):
                for metric in self._metrics:
                    metric.log_custom_summaries()
                    metric._rollback_states()

        with self._train_writer.as_default(step=epoch):
            for metric in self._metrics:
                metric.log_custom_summaries()

    def on_train_end(self, logs=None):
        try:
            tf.summary.flush(writer=self._train_writer)
            tf.summary.flush(writer=self._val_writer)
        except Exception:
            traceback.print_exc()


class NaNLossDebugger(tf.keras.callbacks.Callback):
    def __init__(self, logdir, debug_mode='FULL_HEALTH', stack_depth=1024):
        self._logdir = logdir
        self._debugger = tf.debugging.experimental.enable_dump_debug_info(
            self._logdir, tensor_debug_mode=debug_mode, circular_buffer_size=stack_depth
        )
        self._debugger.FlushNonExecutionFiles()

    def _call(self, logs):
        if tf.math.is_finite(logs['loss']):
            return

        self._debugger.FlushExecutionFiles()
        raise RuntimeError(f"NaNs in loss encountered. See {self._logdir} for TF debugging files.")

    def on_train_batch_end(self, batch, logs=None):
        self._call(logs)

    def on_test_batch_end(self, batch, logs=None):
        self._call(logs)
