import logging
import tempfile
from typing import Iterator, List, Optional

import numpy as np
import tensorflow as tf
from PIL import Image

from hawran import dataset
from hawran.charset import Charset
from hawran.typesetting import Distortions, Inscription


logger = logging.getLogger(__name__)


class DummyCRNN(tf.keras.Model):
    def __init__(self, charset: Charset):
        super().__init__()

        self._charset = charset
        self._model = tf.keras.Sequential([tf.keras.layers.Dense(units=1 + len(self._charset))])
        self._loss_tracker = tf.keras.metrics.Mean(name='loss')

    @property
    def metrics(self):
        return [self._loss_tracker]

    def call(self, inputs, training=None, mask=None):
        x = tf.squeeze(inputs[:, :, ::4, :], axis=-1)
        return self._model(x, training=training, mask=mask)

    def _loss(self, data, logits):
        return tf.math.reduce_sum(logits)

    def train_step(self, data):
        with tf.GradientTape() as tape:
            logits = self(data['img'], training=True)
            loss = self._loss(data, logits)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Update metrics
        self._loss_tracker.update_state(loss)

        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}

    def test_step(self, data):
        # Compute predictions
        logits = self(data['img'], training=False)
        # Updates the metrics tracking the loss
        loss = self._loss(data, logits)
        # Update the metrics.
        self._loss_tracker(loss)
        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}


class CRNN(tf.keras.Model):
    INPUT_HEIGHT_PX: int = 48
    HORIZONTAL_STRIDE: int = 2

    """
    Evolved from https://arxiv.org/pdf/1507.05717.pdf
    """
    def __init__(self, charset: Charset):
        assert charset.blank_idx == 0

        super().__init__()

        self._charset = charset
        self._loss_tracker = tf.keras.metrics.Mean(name='loss')

        self._cnn = tf.keras.Sequential([
            tf.keras.layers.Conv2D(
                filters=64,
                kernel_size=3,
                strides=1,
                padding='same',
                activation=None,
                input_shape=(self.INPUT_HEIGHT_PX, None, 1)
            ),
            *self.BnReLUConv(filters=64, kernel_size=3, strides=2, padding='same'),
            *self.BnReLUConv(filters=128, kernel_size=3, strides=(2, 1), padding='same'),
            *self.BnReLUConv(filters=128, kernel_size=3, strides=1, padding='same'),
            *self.BnReLUConv(filters=256, kernel_size=3, strides=(2, 1), padding='same'),
            *self.BnReLUConv(filters=256, kernel_size=3, strides=(2, 1), padding='same'),
            tf.keras.layers.ZeroPadding2D(padding=((0, 0), (1, 1))),
            *self.BnReLUConv(
                filters=512,
                kernel_size=3,
                strides=1,
                padding='valid',
                activation='relu',
                use_bias=True
            ),
        ])

        self._cnn_to_rnn = tf.keras.Sequential([
            tf.keras.layers.Input((1, None, 512), dtype=tf.float32),
            tf.keras.layers.Reshape((-1, 512)),
        ])

        seq_mask = tf.keras.layers.Input((None,), dtype=tf.bool)
        rnn_input = tf.keras.layers.Input((None, 512), dtype=tf.float32)
        rnn_input_dropped = tf.keras.layers.Dropout(0.25)(rnn_input)
        rnn_1 = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(units=256, return_sequences=True), merge_mode='concat',
        )(rnn_input_dropped, mask=seq_mask)
        rnn_1_dropped = tf.keras.layers.Dropout(0.25)(rnn_1)
        rnn_2 = tf.keras.layers.Bidirectional(
            tf.keras.layers.LSTM(units=256, return_sequences=True), merge_mode='concat',
        )(rnn_1_dropped, mask=seq_mask)
        rnn_2_dropped = tf.keras.layers.Dropout(0.25)(rnn_2)
        rnn_output = rnn_1_dropped + rnn_2_dropped
        self._rnn = tf.keras.Model(inputs=[rnn_input, seq_mask], outputs=rnn_output)

        self._classifier = tf.keras.Sequential([
            tf.keras.layers.Input((None, 512), dtype=tf.float32),
            tf.keras.layers.Dense(units=(1 + len(self._charset))),
        ])


    def BnReLUConv(
        self, filters, kernel_size, strides, padding, activation=None, dropout=0.25, use_bias=False
    ):
        return [
            tf.keras.layers.BatchNormalization(),
            tf.keras.layers.Activation('relu'),
            *([tf.keras.layers.SpatialDropout2D(dropout)] if dropout else []),
            tf.keras.layers.Conv2D(
                filters=filters,
                kernel_size=kernel_size,
                strides=strides,
                padding=padding,
                activation=activation,
                use_bias=use_bias,
            ),
        ]

    @property
    def metrics(self):
        return [self._loss_tracker]

    def call(self, inputs, training=None):
        img, img_width = inputs
        img = tf.cast(img[..., None], dtype=tf.float32) / 128 - 1
        mask = tf.sequence_mask(img_width // self.HORIZONTAL_STRIDE, dtype=tf.bool)

        cnn_outs = self._cnn(img, training=training)
        rnn_ins = self._cnn_to_rnn(cnn_outs)
        rnn_outs = self._rnn([rnn_ins, mask], training=training)
        characters = self._classifier(rnn_outs, training=training)
        logits = characters * tf.cast(mask, dtype=tf.float32)[..., None]
        return logits

    def _greedy_decode(self, data, logits):
        (decoded,), _ = tf.nn.ctc_greedy_decoder(
            tf.transpose(tf.roll(logits, -1, 2), perm=(1, 0, 2)),
            data['img_width'] // self.HORIZONTAL_STRIDE,
        )
        return tf.cast(
            tf.sparse.to_dense(
                tf.SparseTensor(decoded.indices, decoded.values + 1, decoded.dense_shape)
            ),
            dtype=tf.int32
        )

    def _loss(self, data, logits):
        return tf.nn.ctc_loss(
            tf.sparse.from_dense(tf.cast(data['labels'], dtype=tf.int32)),
            logits,
            None,
            data['img_width'] // self.HORIZONTAL_STRIDE,
            logits_time_major=False,
            blank_index=0,
        )

    def _loss_step(self, data, logits):
        loss = self._loss(data, logits)
        self._loss_tracker.update_state(loss)
        return loss

    def train_step(self, data):
        with tf.GradientTape() as tape:
            logits = self([data['img'], data['img_width']], training=True)
            loss = self._loss_step(data, logits)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}

    def test_step(self, data):
        # Compute predictions
        logits = self([data['img'], data['img_width']], training=False)
        # Updates the metrics tracking the loss
        _ = self._loss_step(data, logits)

        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        return {m.name: m.result() for m in self.metrics}

    def predict_step(self, data):
        logits = self([data['img'], data['img_width']], training=False)
        return logits

    def predict_ds(self, ds, batch_size: int = 64):
        return PredictionsDataset(self, ds, batch_size)


class PreTrainedCRNN(CRNN):
    def __init__(self, charset):
        super().__init__(charset)
        self._cnn.trainable = False
        self._cnn_to_rnn.trainable = False
        self._rnn.trainable = False


class PredictionsDataset(dataset.TransformedDataset):
    def __init__(self, crnn, samples_ds, batch_size: int = 64):
        super().__init__(source=ExamplesDataset(samples_ds, crnn._charset))
        self._batch_size = batch_size
        self._crnn = crnn

    def _iter_samples(self):
        yield from tfds_from_ds(self._source, self._batch_size)

    def _transform(self, **batch) -> dict:
        logits = self._crnn.predict_step(batch)

        for ex_i in range(len(logits)):
            img_width = batch['img_width'][ex_i].numpy()
            strided_width = img_width // self._crnn.HORIZONTAL_STRIDE
            label_width = batch['label_width'][ex_i].numpy()
            yield {
                'example_id': batch['example_id'][ex_i].numpy().decode('utf-8'),
                'img': batch['img'][ex_i, :, :img_width].numpy(),
                'transliteration': self._crnn._charset.indices_to_str(
                    batch['labels'][ex_i, :label_width].numpy()
                ),
                'logits': logits[ex_i, :strided_width].numpy(),
            }


class ExamplesDataset(dataset.TransformedDataset):
    def __init__(
        self,
        samples_ds: dataset.Dataset,
        charset: Charset,
        height: int = None,
        horizontal_stride: int = None,
        max_width: int = 1024,
    ):
        super().__init__(source=samples_ds)
        self._charset = charset
        self._height = height or CRNN.INPUT_HEIGHT_PX
        self._horizontal_stride = horizontal_stride or CRNN.HORIZONTAL_STRIDE
        self._max_width = max_width

    def _transform(
        self,
        example_id: str,
        img: np.ndarray,
        transliteration: Optional[str] = None,
    ) -> Iterator[dict]:
        h, w = img.shape
        w_padding = ((- w) % self._horizontal_stride)
        if (h != self._height) or w_padding:
            canvas = np.zeros((self._height, w + w_padding), dtype=np.uint8)

            y = (h - self._height) // 2
            img = img[max(0, y):(y + self._height)]
            top = max(0, -y)
            canvas[top:(top + img.shape[0]), :img.shape[1]] = img

            img = canvas
        img = img[:, :self._max_width]

        labels = np.array(
            [
                idx
                for _, _, idxs in self._charset.transcribe(transliteration or '')
                for idx in idxs
            ],
            dtype=np.uint8,
        )

        extended_labelling_len = labels.size + (labels[:-1] == labels[1:]).sum()
        strided_width = img.shape[1] // self._horizontal_stride
        if strided_width < extended_labelling_len:
            logger.debug(
                "Invalid example encountered - extended labelling does not exist; "
                f"shape={img.shape}, labels={transliteration}"
            )
            return

        yield {
            'example_id': example_id,
            'img': img,
            'labels': labels,
            'label_width': np.array(labels.size, dtype=np.uint8),
            'img_width': np.array(img.shape[1], dtype=np.int32),
        }


def make_ds(
    shard_names: List[str],
    font_names: List[str],
    max_width: int,
    charset: Charset,
    epoch_samples: int,
    seed: int = None,
):
    shard_names_ds = dataset.ListDataset.from_components(shard_name=shard_names)
    shard_names_ds = dataset.RepeatDataset(shard_names_ds)
    shard_names_ds = dataset.ShuffleDataset.maybe(
        source=shard_names_ds, buffer_len=len(shard_names), seed=seed
    )
    tokens_ds = dataset.SYNv4ShardTokensDataset(shard_names_ds)
    tokens_ds = dataset.ShuffleDataset.maybe(tokens_ds, buffer_len=epoch_samples, seed=seed)
    texts_ds = dataset.SpacifiedTokensDataset(tokens_ds)

    font_names_ds = dataset.ListDataset.from_components(font_name=font_names)
    font_names_ds = dataset.RepeatDataset(font_names_ds)
    fonts_ds = dataset.HandwrittenFontsDataset(
        font_names_ds,
        rotation_range=(-8, 8),
        scale_x_range=(0.5, 1.5),
        scale_y_range=(0.75, 1.25),
        slant_range=(-45, 30),
        weight_range=(-0.5, 0.5),
        max_inconsistency=0.1,
        n_variants=4,
        seed=(seed or 666),
    )

    if seed is not None:
        fonts_ds = dataset.FadeOutCache(fonts_ds, 4, 80)  # TODO parametrize properly

    scripts_ds = dataset.ZipDataset(texts_ds, fonts_ds)

    glyphs_ds = dataset.GlyphsDataset(scripts_ds, 32, charset)  # TODO parametrize height

    inscriptions_ds = dataset.InscriptionsDataset(
        glyphs_ds,
        line_len=((7 * max_width / 8), max_width),
        preserve_words=True,
        leading=CRNN.INPUT_HEIGHT_PX // 2,
        collate_prob=0.8,
        seed=(seed or 666),
    )

    if seed is not None:
        inscriptions_ds = dataset.FadeOutCache(inscriptions_ds, 4, epoch_samples)

    samples_ds = dataset.InscriptionSamplesDataset(
        inscriptions_ds,
        CRNN.INPUT_HEIGHT_PX,
        CRNN.HORIZONTAL_STRIDE,
        seed=(seed or 666),
    )
    samples_ds = dataset.ShuffleDataset.maybe(
        samples_ds, buffer_len=epoch_samples, seed=seed
    )
    examples_ds = ExamplesDataset(samples_ds, charset=charset)

    return examples_ds


def make_train_ds(
    shard_names: List[str],
    font_names: List[str],
    charset: Charset,
    max_width: int,
    batch_size: int,
    steps: int,
    seed: int,
):
    epoch_samples = steps * batch_size
    ds = tf.data.Dataset.from_generator(
        lambda: make_ds(shard_names, font_names, max_width, charset, epoch_samples, seed),
        output_signature={
            'example_id': tf.TensorSpec(shape=(), dtype=tf.string),
            'img': tf.TensorSpec(
                shape=(CRNN.INPUT_HEIGHT_PX, None), dtype=tf.uint8
            ),
            'labels': tf.TensorSpec(shape=(None,), dtype=tf.uint8),
            'label_width': tf.TensorSpec(shape=(), dtype=tf.uint8),
            'img_width': tf.TensorSpec(shape=(), dtype=tf.int32),
        },
    )
    ds = ds.padded_batch(batch_size)
    ds = ds.prefetch(4)

    return ds


def make_val_ds(
    shard_names: List[str],
    font_names: List[str],
    charset: Charset,
    max_width: int,
    batch_size: int,
    steps: int,
):
    ds = tf.data.Dataset.from_generator(
        lambda: make_ds(shard_names, font_names, max_width, charset, steps * batch_size),
        output_signature={
            'example_id': tf.TensorSpec(shape=(), dtype=tf.string),
            'img': tf.TensorSpec(
                shape=(CRNN.INPUT_HEIGHT_PX, None), dtype=tf.uint8
            ),
            'labels': tf.TensorSpec(shape=(None,), dtype=tf.uint8),
            'label_width': tf.TensorSpec(shape=(), dtype=tf.uint8),
            'img_width': tf.TensorSpec(shape=(), dtype=tf.int32),
        },
    )
    ds = ds.padded_batch(batch_size)
    ds = ds.take(steps)
    ds = ds.cache(tempfile.mkdtemp() + '/tfcache')
    ds = ds.prefetch(4)

    return ds


def get_real_ds(archive_names: List[str], charset: Charset):
    archive_names_ds = dataset.ListDataset.from_components(
        archive_name=archive_names
    )
    pages_ds = dataset.RealPagesDataset(archive_names_ds)
    samples_ds = dataset.RealSamplesDataset(pages_ds, CRNN.INPUT_HEIGHT_PX)
    examples_ds = ExamplesDataset(samples_ds, charset)
    return examples_ds


def tfds_from_ds(
    examples_ds: ExamplesDataset,
    batch_size: int,
    n_steps: int = None,
    cache: bool = False,
):
    ds = tf.data.Dataset.from_generator(
        lambda: examples_ds,
        output_signature={
            'example_id': tf.TensorSpec(shape=(), dtype=tf.string),
            'img': tf.TensorSpec(shape=(CRNN.INPUT_HEIGHT_PX, None), dtype=tf.uint8),
            'labels': tf.TensorSpec(shape=(None,), dtype=tf.uint8),
            'label_width': tf.TensorSpec(shape=(), dtype=tf.uint8),
            'img_width': tf.TensorSpec(shape=(), dtype=tf.int32),
        }
    )
    ds = ds.padded_batch(batch_size)
    if n_steps:
        ds = ds.take(n_steps)
    if cache:
        ds = ds.cache(tempfile.mkdtemp() + '/tfcache')
    ds = ds.prefetch(4)

    return ds


def dummy_dataset(charset, batch_size, img_height, text_len, length=None):
    img_width = text_len * 8
    example = {
        'img': np.random.random((img_height, img_width)).astype(dtype=np.uint8),
        'labels': np.random.choice(len(charset) + 1, text_len).astype(np.uint8),
        'char_map': np.random.choice(len(charset) + 1, img_width).astype(np.int32),
        'img_width': np.array(img_width, dtype=np.int32),
        'label_width': np.array(text_len, dtype=np.int32),
    }
    return tf.data.Dataset.from_tensors(example).repeat(length).batch(batch_size)
