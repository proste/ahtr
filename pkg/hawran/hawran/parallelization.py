import multiprocessing
import time
import traceback


mp_fs = multiprocessing.get_context('forkserver')


class Synthetizer:
    def __init__(self, name):
        self.name = name

    def synthetize(self):
        raise NotImplementedError

    def __iter__(self):
        counter = 0
        while True:
            t = time.perf_counter()
            result = self.synthetize()
            yield self.name, counter, t, result
            counter += 1

    def __repr__(self):
        return f'{self.__class__.__name__}(name={self.name})'


class IOSynthetizer(Synthetizer):
    def __init__(self, name, delay=0.2):
        super().__init__(name)
        self.delay = delay

    def synthetize(self):
        return time.sleep(self.delay)


class BusySynthetizer(Synthetizer):
    def __init__(self, name, delay=0.2):
        super().__init__(name)
        self.delay = delay

    def synthetize(self):
        clk_id = time.CLOCK_THREAD_CPUTIME_ID
        ct = time.clock_gettime(clk_id)
        while (time.clock_gettime(clk_id) - ct) < self.delay:
            pass


class _IteratorProcess(mp_fs.Process):
    def __init__(self, iterable_init, args, kwargs, connection, prefetch):
        super().__init__()
        self.iterable_init = iterable_init
        self.args = args or []
        self.kwargs = kwargs or {}
        self.connection = connection
        self.prefetch = prefetch

    def run(self):
        try:
            iterable = self.iterable_init(*self.args, **self.kwargs)
            it = iter(iterable)

            sent_count = 0
            while True:
                if sent_count >= self.prefetch:
                    msg = self.connection.recv()
                else:
                    msg = None

                while True:
                    if msg == 'ACK':
                        sent_count -= 1
                    elif msg == 'EOF':
                        return

                    if not self.connection.poll():
                        break
                    else:
                        msg = self.connection.recv()

                try:
                    self.connection.send(('SYN', next(it)))
                    sent_count += 1
                except StopIteration:
                    self.connection.send(('EOF', None))
                    return
        except Exception:
            self.connection.send(('ERR', traceback.format_exc()))


class ProcessIterator:
    def __init__(self, iterable_init, args=None, kwargs=None, prefetch=1):
        self.parent_conn, child_conn = mp_fs.Pipe()
        self.process = _IteratorProcess(iterable_init, args, kwargs, child_conn, prefetch)
        self.process.start()

    def __iter__(self):
        return self

    def __next__(self):
        kind, elem = self.parent_conn.recv()
        if kind == 'EOF':
            raise StopIteration
        elif kind == 'ERR':
            raise RuntimeError(elem)
        elif kind == 'SYN':
            self.parent_conn.send('ACK')
            return elem
        else:
            raise ValueError

    def stop(self):
        self.parent_conn.send('EOF')
        self.process.join()

    def __del__(self):
        self.stop()
