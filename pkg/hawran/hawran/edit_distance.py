import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

from hawran.charset import Charset
from hawran.plot import heatmap, fig_to_image
from hawran.tensorflow_utils import CustomMetric


@tf.function
def _init_costs(true, pred, op_costs, condensed):
    batch_size = len(true)
    p_max, t_max = tf.shape(pred)[-1] + 1, tf.shape(true)[-1] + 1

    if condensed:
        init_true = tf.cast(true != 0, dtype=tf.int32)
        init_pred = tf.cast(pred != 0, dtype=tf.int32)
    else:
        init_true = true
        init_pred = pred

    add_costs = tf.cumsum(tf.gather(op_costs[0, :, 2], init_true), axis=-1)
    del_costs = tf.cumsum(tf.gather(op_costs[:, 0, 1], init_pred), axis=-1)[:, :, None]

    return tf.transpose(
        tf.concat(
            (
                tf.concat(
                    (tf.repeat(op_costs[0, 0, 0], batch_size)[:, None], add_costs),
                    axis=-1
                )[:, None, :],
                tf.concat(
                    (del_costs, tf.zeros((batch_size, p_max - 1, t_max - 1), dtype=tf.int32)),
                    axis=2
                ),
            ),
            axis=1
        ),
        perm=(1, 2, 0)
    )


@tf.function
def _init_ops(p_max, t_max, batch_size):
    return tf.repeat(
        tf.concat(
            (
                tf.concat(([0], tf.repeat(2, t_max - 1)), axis=0)[None, :],
                tf.concat(
                    (tf.repeat(1, p_max - 1)[:, None], tf.zeros((p_max - 1, t_max - 1), dtype=tf.int32)),
                    axis=1
                ),
            ),
            axis=0
        )[:, :, None],
        batch_size,
        axis=-1
    )


@tf.function
def forward_pass(true, pred, op_costs, condensed):
    batch_size = len(true)  # assert batch size same for pred
    p_max, t_max = tf.shape(pred)[-1] + 1, tf.shape(true)[-1] + 1

    costs = _init_costs(true, pred, op_costs, condensed)
    ops = _init_ops(p_max, t_max, batch_size)

    # TODO consider speed up using diagonal wave
    for p_i in tf.range(1, p_max):
        for t_i in tf.range(1, t_max):
            curr_true = true[:, t_i - 1]
            curr_pred = pred[:, p_i - 1]

            if condensed:
                curr_true, curr_pred = (
                    tf.cast(curr_true != 0, dtype=tf.int32),
                    (
                        tf.cast(curr_pred != 0, dtype=tf.int32)
                        * (1 + tf.cast(curr_true != curr_pred, dtype=tf.int32))
                    )
                )

            indices = tf.stack((curr_pred, curr_true), axis=-1)
            curr_costs = tf.gather_nd(op_costs, indices)
            prev_costs = tf.stack(
                (costs[p_i - 1, t_i - 1, :], costs[p_i - 1, t_i, :], costs[p_i, t_i - 1, :]),
                axis=-1
            )

            joined_costs = prev_costs + curr_costs

            best_op = tf.argmin(joined_costs, axis=-1, output_type=tf.int32)
            best_cost = tf.gather(joined_costs, best_op, batch_dims=1)

            ops = tf.tensor_scatter_nd_update(ops, [[p_i, t_i]], [best_op])
            costs = tf.tensor_scatter_nd_update(costs, [[p_i, t_i]], [best_cost])

    return tf.transpose(costs, perm=(2, 0, 1)), tf.transpose(ops, perm=(2, 0, 1))


@tf.function
def backward_pass(true, pred, ops):
    batch_size = len(true)  # assert batch size same for pred and ops
    true = tf.concat((tf.zeros((batch_size, 1), dtype=tf.int32), true), axis=-1)
    pred = tf.concat((tf.zeros((batch_size, 1), dtype=tf.int32), pred), axis=-1)
    p_max, t_max = tf.shape(pred)[-1], tf.shape(true)[-1]

    backstep = tf.convert_to_tensor(
        [
            [-1, -1],
            [-1,  0],
            [ 0, -1]
        ],
        dtype=tf.int32,
    )

    # TODO tighter bound on max steps
    max_len = p_max + t_max
    paths = tf.zeros((max_len, batch_size), dtype=tf.int32)
    symbols = tf.zeros((max_len, 2, batch_size), dtype=tf.int32)

    step_idx = p_max + t_max - 1
    indices = tf.ones((batch_size, 2), dtype=np.int32) * [p_max - 1, t_max - 1]
    while tf.reduce_any(indices > 0):
        curr_ops = tf.gather_nd(ops, indices, batch_dims=1)
        curr_symbols = tf.stack(
            (
                tf.gather(pred, indices[:, 0], batch_dims=1),
                tf.gather(true, indices[:, 1], batch_dims=1)
            ),
            axis=0
        )

        indices = tf.math.maximum(0, indices + tf.gather(backstep, curr_ops))

        paths = tf.tensor_scatter_nd_update(paths, [[step_idx]], curr_ops[None, :])
        symbols = tf.tensor_scatter_nd_update(symbols, [[step_idx]], curr_symbols[None, :])
        step_idx -= 1

    paths = tf.transpose(paths, perm=(1, 0))
    symbols = tf.transpose(symbols, perm=(2, 0, 1))

    begin_idx = tf.reduce_sum(tf.cast(tf.reduce_sum(symbols, axis=(0, 2)) == 0, tf.int32))

    return paths[:, begin_idx:], symbols[:, begin_idx:, :]


class EditDistance(CustomMetric):
    @staticmethod
    def default_op_costs():
        return tf.convert_to_tensor(
            [
                [[0, 0, 0], [2, 0, 1], [2, 0, 1]],
                [[2, 1, 0], [0, 1, 1], [1, 1, 1]],
                [[2, 1, 0], [1, 1, 1], [0, 1, 1]],
            ],
            dtype=tf.int32
        ), True

    def __init__(self, charset: Charset, op_costs, condensed, name='edit_dist'):
        assert charset.blank_idx == 0

        super().__init__(name=name)

        # TODO asserts on condensed/op_costs/charset size compatibility
        self.symbols = charset.out_chars
        self.op_costs = op_costs
        self.condensed = condensed

        self.distance_acc = self.add_weight('distance_acc', initializer='zeros', dtype=tf.float32)
        self.sample_count = self.add_weight('sample_count', initializer='zeros', dtype=tf.int32)

        n_symbols = 1 + len(self.symbols)
        self.op_counts = self.add_weight(
            'op_counts', shape=(n_symbols, n_symbols, 3), initializer='zeros', dtype=tf.int32
        )

        # TODO(proste): parametrize (derive from charset size and target resolution)
        self.figsize = (33.33, 21.5)

    def update_state(self, true, pred):
        with tf.device('/CPU:0'):
            costs, best_ops = forward_pass(true, pred, self.op_costs, self.condensed)
            paths, symbols = backward_pass(true, pred, best_ops)

            # TODO(proste): consider passing in sequence lengths
            seq_lengths = tf.reduce_sum(tf.cast(true > 0, dtype=tf.float32), axis=-1)
            normalized_distances = tf.cast(costs[:, -1, -1], dtype=tf.float32) / seq_lengths
            self.distance_acc.assign_add(tf.reduce_sum(normalized_distances))
            self.sample_count.assign_add(len(true))

            indices = tf.concat((symbols, paths[:, :, None]), axis=-1)
            self.op_counts.scatter_nd_add(indices, tf.ones_like(indices[:, :, 0], dtype=tf.int32))

    def plot(self):
        uses = self.op_counts[1:, 1:, 0]
        dels = tf.reduce_sum(self.op_counts[1:, :, 1], axis=1)
        adds = tf.reduce_sum(self.op_counts[:, 1:, 2], axis=0)

        total = tf.maximum(1, tf.reduce_sum(uses) + tf.reduce_sum(adds))
        uses_pm = 1000 * uses / total
        dels_pm = 1000 * dels / total
        adds_pm = 1000 * adds / total
        dist_pm = 1000 * self.result()

        w, h = self.figsize
        n_symbols = len(self.symbols)
        grid_size = 1 / (n_symbols + 5.1)
        spacing = 0.1 * grid_size
        main_size = n_symbols * grid_size
        off_size = grid_size
        origo = 2 * grid_size

        uses_loc = [origo, origo + off_size + spacing, main_size, main_size]
        dels_loc = [origo + main_size + spacing, origo + off_size + spacing, off_size, main_size]
        adds_loc = [origo, origo, main_size, off_size]
        mean_loc = [origo + main_size + spacing, origo, off_size, off_size]

        fig = plt.Figure(figsize=(w, h), dpi=64)
        uses_ax = fig.add_axes(uses_loc)
        uses_ax.set_xticks(np.arange(n_symbols))
        uses_ax.set_xticks(np.arange(n_symbols - 1) + 0.5, minor=True)
        uses_ax.set_xticklabels(self.symbols)
        uses_ax.set_yticks(np.arange(n_symbols))
        uses_ax.set_yticks(np.arange(n_symbols - 1) + 0.5, minor=True)
        uses_ax.set_yticklabels(self.symbols)
        dels_ax = fig.add_axes(dels_loc, sharey=uses_ax)
        adds_ax = fig.add_axes(adds_loc, sharex=uses_ax)
        mean_ax = fig.add_axes(mean_loc)

        adds_ax.set_yticks([])
        adds_ax.set_ylabel('ADD')
        adds_ax.yaxis.set_label_position('left')
        dels_ax.set_xticks([])
        dels_ax.yaxis.tick_right()
        dels_ax.set_xlabel('DEL')
        dels_ax.xaxis.set_label_position('top')
        uses_ax.xaxis.tick_top()
        mean_ax.xaxis.set_visible(False)
        mean_ax.yaxis.set_visible(False)
        uses_ax.grid(which='minor', color='#EEEEEE')

        heatmap(uses_pm.numpy(), uses_ax, 'Blues', vmin=1, fmt='{:.0f}')
        heatmap(adds_pm.numpy()[None, :], adds_ax, 'Greens', vmin=1, fmt='{:.0f}')
        heatmap(dels_pm.numpy()[:, None], dels_ax, 'Reds', vmin=1, fmt='{:.0f}')
        heatmap(dist_pm.numpy()[None, None], mean_ax, 'inferno', vmin=0, vmax=1, fmt='{:.0f}')

        return fig

    def result(self):
        return self.distance_acc / tf.cast(self.sample_count, dtype=tf.float32)

    def log_custom_summaries(self):
        fig = self.plot()
        img = fig_to_image(fig)
        tf_img = tf.image.decode_png(img, channels=4)

        tf.summary.image(self.name, tf_img[None, ...])


def print_diff(paths, symbols, op_costs, condensed, charset: Charset = None):
    charset = ['∅'] + list(charset.out_chars)
    for path, symbols_ in zip(paths, symbols):
        cost = 0
        for op, symbol in zip(path, symbols_):
            if condensed:
                p_sym = int(symbol[0] != 0)
                t_sym = (1 + int(symbol[0] != symbol[1])) * int(symbol[1] != 0)
                op_cost = op_costs[p_sym, t_sym, op]
            else:
                op_cost = op_costs[symbol[0], symbol[1], op]

            cost += op_cost
            p_char, t_char = (charset[symbol[0]], charset[symbol[1]]) if charset else symbol
            if op == 0:
                if 0 in symbol:
                    desc = None
                elif symbol[0] == symbol[1]:
                    desc = f'   {p_char} {t_char}  {op_cost}'
                else:
                    desc = f'-+ {p_char} {t_char}  {op_cost}'
            elif op == 1:
                if symbol[0] == 0:
                    desc = None
                else:
                    desc = f'-  {p_char}    {op_cost}'
            else:
                if symbol[1] == 0:
                    desc = None
                else:
                    desc = f' +   {t_char}  {op_cost}'

            if desc:
                print(desc)
        print('-----------')
        print(f'        {cost}')
        print()


def diff(true, pred, op_costs, condensed, charset=None):
    costs, ops = forward_pass(true, pred, op_costs, condensed)
    paths, symbols = backward_pass(true, pred, ops)
    print_diff(paths, symbols, op_costs, condensed, charset)
