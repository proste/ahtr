import csv
import logging
import lzma

from tqdm import tqdm

from hawran import storage


logger = logging.getLogger(__file__)


def size_tqdm(it, **kwargs):
    tqdm_ = tqdm(**kwargs)
    for element in it:
        tqdm_.update(len(element))
        yield element
    tqdm_.close()


class Document:
    @staticmethod
    def parse_attributes(attribute_str):
        attribute_tokens = attribute_str.split('"')[:-1]
        keys = (token.strip(' =') for token in attribute_tokens[::2])
        values = attribute_tokens[1::2]
        return dict(zip(keys, values))

    @classmethod
    def parse_line(cls, line):
        if (line[0] == '<') and (line[-1] == '>'):
            if line[1] != '/':
                tag, *attribute_str = line[1:-1].split(' ', maxsplit=1)
                attributes = cls.parse_attributes(attribute_str[0]) if attribute_str else {}

                return tag, attributes, None
            else:
                return line[2:-1], None, None
        else:
            return None, None, line

    def __init__(self, it):
        self._it = it

        doc_tag, doc_attributes, doc_text = self.parse_line(next(self._it))
        assert doc_tag == 'doc'
        assert doc_text is None
        self.attributes = doc_attributes

        self.content = []

        s_id = None
        block_i = None
        builder = None
        for tag, attributes, text in map(self.parse_line, self._it):
            if tag:
                if attributes is not None:
                    if tag == 'block':
                        block_i += 1
                    elif tag == 'doc':
                        raise ValueError
                    elif tag == 'p':
                        assert self.attributes['id'] == attributes['id'].split(':')[0]
                        block_i = 0
                    elif tag == 's':
                        s_id = attributes['id']
                        builder = [f"{self.attributes['id']}:{block_i}:{s_id}"]
                else:
                    if tag == 'doc':
                        self._it = None
                        break
                    elif tag == 'p':
                        block_i = None
                    elif tag == 's':
                        if len(builder) > 1:
                            self.content.append('\t'.join(builder))
                        s_id = None
                        builder = None
            else:
                word, *_ = text.split('\t', maxsplit=1)
                builder.append(word)

        n_blocks = self.content[-1].split('\t')[0].split(':')[1]
        n_sentences = len(self.content)
        self.attributes.update({'n_blocks': int(n_blocks), 'n_sentences': int(n_sentences)})

    def __len__(self):
        return sum(map(len, self.content))


def doc_generator(path):
    with open(path, 'r') as syn_f:
        syn_it = map(str.rstrip, size_tqdm(syn_f, unit='B', unit_scale=True))

        while True:
            try:
                doc = Document(syn_it)
                yield doc
            except StopIteration:
                break


class ShardedIterator:
    def __init__(self, it, max_size):
        self._it = it
        self._max_size = max_size
        self._last_item = next(self._it)

    def _shard_iter(self):
        yield self._last_item
        curr_size = len(self._last_item)

        for self._last_item in self._it:
            curr_size += len(self._last_item)
            if curr_size > self._max_size:
                break
            yield self._last_item
        else:
            self._last_item = None

    def __iter__(self):
        while True:
            yield self._shard_iter()
            if self._last_item is None:
                break


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('in_path')
    parser.add_argument('shard_template')
    parser.add_argument('--shard-size-mib', type=int, default=512)
    parser.add_argument('--compression', type=int, default=2)
    args = parser.parse_args()

    doc_gen = doc_generator(args.in_path)
    shards_it = iter(ShardedIterator(doc_gen, args.shard_size_mib * (2 ** 20)))

    with storage.open(args.shard_template.format('index', 'csv'), 'wt', newline='') as index_f:
        writer = None
        for shard_idx, shard_it in enumerate(shards_it):
            path = 'texts/' + args.shard_template.format(shard_idx, 'xz')
            with storage.open(path, 'wb') as shard_f:
                with lzma.open(shard_f, mode='wt', preset=args.compression) as xz_f:
                    for doc in shard_it:
                        doc_index = {**doc.attributes, 'shard': path}
                        if writer is None:
                            writer = csv.DictWriter(
                                index_f,
                                fieldnames=list(doc_index.keys()),
                                dialect=csv.unix_dialect,
                            )
                            writer.writeheader()
                        writer.writerow(doc_index)

                        xz_f.writelines(token for line in doc.content for token in (line, '\n'))
            logger.info(f'Shard {path} created and uploaded.')
