import json
import os
from collections import defaultdict

import lzma
from tqdm import tqdm

from hawran import storage


if __name__ == '__main__':
    experiment_name = os.environ['EXPERIMENT_NAME']

    shard_names = sorted(
        shard_name
        for shard_name in storage.list('texts/syn_v4').keys()
        if shard_name.endswith('xz')
    )

    charset = set()
    counts = defaultdict(int)
    news = {}

    shard_to_doc_ids = {}
    for shard_name in tqdm(shard_names):
        with storage.open(shard_name, 'rb') as xz_f:
            with lzma.open(xz_f, mode='rt') as shard_f:
                for line in shard_f:
                    sid, sentence = line.rstrip('\n').split('\t', maxsplit=1)
                    sentence_set = set(sentence)

                    new = sentence_set - charset
                    if new:
                        news[sid] = sentence

                        spacified = ''.join(spacify(sentence.split('\t')))
                        print(new, sid, spacified)

                        charset.update(sentence_set)

                    for char in sentence:
                        counts[char] += 1

    with storage.open('texts/syn_v4.charset.json', 'w') as charset_f:
        json.dump(counts, charset_f, indent=2, sort_keys=True)

    with storage.open(f'experiments/{experiment_name}/news.json', 'w') as news_f:
        json.dump(news, news_f, indent=2, ensure_ascii=False)
