import logging
import os

import numpy as np
import tensorflow as tf

from hawran import storage
from hawran.crnn import CRNNCombined
from hawran.dataset import charset, dummy_dataset, FixedHeightRandomInscriptionSynthetizer
from hawran.tensorflow_utils import NaNLossDebugger


logger = logging.getLogger(__name__)

experiment_name = os.environ['EXPERIMENT_NAME']
physical_devices = tf.config.list_physical_devices('GPU')
tf.config.experimental.set_memory_growth(physical_devices[0], True)


@tf.function
def normalize(ex):
    return {
        **ex,
        'img': (tf.cast(ex['img'], dtype=tf.float32) - 128) / 128,
        'labels': tf.cast(ex['labels'] + 1, dtype=tf.int32),
    }


def synth_dataset(fonts_dir, charset, batch_size, img_height, text_len, length=None):
    fonts = [
        font_path
        for font_path in storage.list(fonts_dir)
        if font_path.endswith('.ttf')
    ]

    def synthetizer(fonts):
        its = [
            iter(
                FixedHeightRandomInscriptionSynthetizer(
                    text_len, charset, font, img_height
                )
            )
            for font in fonts
        ]

        while True:
            for it in its:
                yield next(it)

    ds = tf.data.Dataset.from_generator(
        lambda fonts: synthetizer(fonts.tolist()),
        output_types={
            'img': tf.uint8,
            'labels': tf.uint8,
            'char_map': tf.uint8,
            'label_width': tf.uint8,
            'img_width': tf.int32,
        },
        output_shapes={
            'img': tf.TensorShape([32, None, 1]),
            'labels': tf.TensorShape([None]),
            'char_map': tf.TensorShape([None]),
            'label_width': tf.TensorShape([]),
            'img_width': tf.TensorShape([]),
        },
        args=(fonts,),
    )
    ds = ds.padded_batch(batch_size)
    ds = ds.map(normalize)

    if length:
        ds = ds.take(length)

    ds = ds.prefetch(4).apply(
        tf.data.experimental.prefetch_to_device(
            physical_devices[0].name.split(':', maxsplit=1)[1]
        )
    )

    return ds


def make_ds(kind, batch_size, text_len, length):
    img_height = 32

    if kind == 'dummy':
        val_ds = dummy_dataset(charset, batch_size, img_height, text_len, length)
    else:
        val_ds = synth_dataset(
            'fonts/val/', charset, batch_size, img_height, text_len, length
        )

    return val_ds


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset', choices=('dummy', 'synth'))
    parser.add_argument('weights', type=str)
    parser.add_argument('--batch-size', type=int, default=64)
    parser.add_argument('--batch-count', type=int, default=256)
    args = parser.parse_args()

    img_height = 32
    epochs = 256
    validation_freq = 4
    text_len = 64

    crnn = CRNNCombined(charset)
    crnn.compile()

    weights_path = f's3://hawran/experiments/{args.weights}'
    crnn.load_weights(weights_path)
    logger.info(f"Loaded weights from {weights_path}")

    val_ds = make_ds(
        args.dataset, args.batch_size, text_len, args.batch_count
    )

    history = crnn.evaluate(
        val_ds,
        callbacks=[
            tf.keras.callbacks.TensorBoard(
                f's3://hawran/tensorboard/{experiment_name}',
            ),
            NaNLossDebugger(f's3://hawran/tensorboard/{experiment_name}'),
        ],
    )
