import io
import tarfile
import random
from time import sleep

from ahmp import Book
from tqdm import tqdm

from hawran import storage


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('permalink')
    parser.add_argument('--zoom', type=int, default=3)
    parser.add_argument('--delay', type=float, default=0.1)
    args = parser.parse_args()

    book = Book.fetch(args.permalink)
    storage_path = f'scans/ahmp_{book.xid}.tar'
    with storage.open(storage_path, 'wb') as storage_f:
        with tarfile.open(fileobj=storage_f, mode='w') as tar_f:
            for page_i in tqdm(range(len(book)), unit='page'):
                _, page_path = book.page(page_i, args.zoom)
                tar_f.add(page_path, f'{page_i:03d}_{args.zoom}.png')
                sleep(args.delay * (random.random() + 4) / 5)

    print(f'Book uploaded to storage under {storage_path}')
