import os

from tqdm import tqdm

from hawran import dataset, storage
from hawran.charset import syn_v4_charset
from hawran.language_modelling import GreedyDecoder, RawBSDecoder, KenLMBSDecoder
from hawran.utils import logprobs_from_logits


def get_greedy_decoder(args):
    return GreedyDecoder(syn_v4_charset.blank_idx)


def get_bs_decoder(args):
    return RawBSDecoder(args.beam_width)


def get_lm_decoder(args):
    with storage.open(args.char_lm, 'rb') as char_f, \
         storage.open(args.word_lm, 'rb') as word_f:
        return KenLMBSDecoder(
            args.beam_width,
            args.pruning,
            char_f.name,
            word_f.name,
            syn_v4_charset,
            args.alpha,
            args.char_beta,
            args.word_beta,
            args.gamma,
        )


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('crnn_predictions')
    parser.add_argument('output_name')
    modes = parser.add_subparsers(dest='mode')

    parser_greedy = modes.add_parser('greedy')
    parser_greedy.set_defaults(get_decoder=get_greedy_decoder)

    parser_bs = modes.add_parser('bs')
    parser_bs.add_argument('--beam-width', type=int, default=64)
    parser_bs.set_defaults(get_decoder=get_bs_decoder)

    parser_lm = modes.add_parser('lm')
    parser_lm.add_argument('char_lm')
    parser_lm.add_argument('word_lm')
    parser_lm.add_argument('--beam-width', type=int, default=64)
    parser_lm.add_argument('--pruning', type=int, default=2)
    parser_lm.add_argument('--alpha', type=float, default=1.2)
    parser_lm.add_argument('--char-beta', type=float, default=0.0)
    parser_lm.add_argument('--word-beta', type=float, default=5.5)
    parser_lm.add_argument('--gamma', type=float, default=1.0)
    parser_lm.set_defaults(get_decoder=get_lm_decoder)

    args = parser.parse_args()

    decoder = args.get_decoder(args)
    predictions_ds = dataset.NPRecordDataset.from_file(args.crnn_predictions)
    decodings_ds = decoder.predict_dataset(predictions_ds)
    decodings_ds = dataset.FilterComponentsDataset(
        decodings_ds, keep=('example_id', 'labels', 'label_widths', 'scores')
    )
    dataset.CacheDataset(decodings_ds, args.output_name).cache()
