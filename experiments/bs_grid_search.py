import json
import os

import Levenshtein
import numpy as np
from tqdm import tqdm

from hawran import storage
from hawran.charset import syn_v4_charset
from hawran.crnn import CRNN, make_val_ds
from hawran.language_modelling import KenLMBSDecoder
from hawran.nprecord import NPRecord
from hawran.utils import logprobs_from_logits


def param_generator(definitions):
    if not definitions:
        yield {}
        return

    (name, (low, high, n)), *definitions = definitions
    for v in np.linspace(low, high, n):
        for params in param_generator(definitions):
            yield {name: v, **params}


def evaluate_cmd(args):
    with storage.open(args.parameters, 'rt') as params_f:
        params = [json.loads(line) for line in params_f]

    with storage.open(args.predictions, 'rb') as pred_f:
        with NPRecord(pred_f, 'r') as pred_npr:
            samples = list(pred_npr)

    results = {}
    for params_i, params_ in enumerate(params):
        if (params_i % args.n_splits) != args.kth_split:
            continue

        with storage.open(args.char_lm, 'rb') as char_f, \
             storage.open(args.word_lm, 'rb') as word_f:
            bs_decoder = KenLMBSDecoder(
                args.beam_width,
                args.pruning,
                char_f.name,
                word_f.name,
                syn_v4_charset,
                params_['alpha'],
                params_['char_beta'],
                params_['word_beta'],
                params_['gamma'],
            )

        acc_edit_distance = 0
        total = 0
        with storage.open(
            f'experiments/{experiment_name}/{params_i}.npr', 'wb'
        ) as dec_f:
            with NPRecord(dec_f, 'w') as dec_npr:
                for sample in bs_decoder.predict_stream(map(
                    lambda sample: {**sample, 'gold_labels': sample['labels']},
                    samples,
                )):

                    dec_npr.append({
                        key: sample[key] for key in (
                            'example_id', 'labels', 'label_widths', 'scores'
                        )
                    })

                    pred_transcription = syn_v4_charset.indices_to_str(
                        sample['labels'][0]
                    )
                    gold_transcription = syn_v4_charset.indices_to_str(
                        sample['gold_labels']
                    )

                    acc_edit_distance += Levenshtein.distance(
                        pred_transcription, gold_transcription
                    )
                    total += len(gold_transcription)

        result = {'mned': acc_edit_distance / total, **params_}
        print(params_i, result)
        results[params_i] = result

    with storage.open(
        f'experiments/{experiment_name}/{args.kth_split}_mned.json', 'wt'
    ) as mneds_f:
        json.dump(results, mneds_f)


def grid_cmd(args):
    with storage.open(args.definitions, 'rt') as def_f:
        definitions = json.load(def_f)

    with storage.open(f'experiments/{experiment_name}/grid.json', 'wt') as grid_f:
        for params in param_generator(definitions.items()):
            grid_f.write(f'{json.dumps(params)}\n')


if __name__ == '__main__':
    experiment_name = os.environ['EXPERIMENT_NAME']

    import argparse
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    parser_evaluate = subparsers.add_parser('evaluate')
    parser_evaluate.add_argument('char_lm')
    parser_evaluate.add_argument('word_lm')
    parser_evaluate.add_argument('predictions')
    parser_evaluate.add_argument('parameters')
    parser_evaluate.add_argument('--kth-split', type=int, default=0)
    parser_evaluate.add_argument('--n-splits', type=int, default=1)
    parser_evaluate.add_argument('--beam_width', type=int, default=64)
    parser_evaluate.add_argument('--pruning', type=int, default=2)
    parser_evaluate.set_defaults(delegate=evaluate_cmd)

    parser_grid = subparsers.add_parser('grid')
    parser_grid.add_argument('definitions')
    parser_grid.set_defaults(delegate=grid_cmd)

    args = parser.parse_args()
    args.delegate(args)
