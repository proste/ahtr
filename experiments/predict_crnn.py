import os

from tqdm import tqdm

from hawran import storage
from hawran.charset import syn_v4_charset
from hawran.crnn import CRNN, get_real_ds, tfds_from_ds
from hawran.nprecord import NPRecord


def load_real_ds(args):
    return get_real_ds([args.archive_name], syn_v4_charset)


def load_synth_ds(args):
    raise NotImplementedError


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('weights')
    parser.add_argument('samples')
    parser.add_argument('output')
    args = parser.parse_args()

    crnn = CRNN(syn_v4_charset)
    crnn.load_weights(f's3://hawran/{args.crnn_weights}')

    samples_ds = args.load_ds(args)
    samples_ds = dataset.CacheDataset.from_file(args.samples)
    predictions_ds = crnn.predict_ds(samples_ds)
    predictions_ds = dataset.FilterComponentsDataset(
        predictions_ds, keep=('example_id', 'logits')
    )
    dataset.CacheDataset(predictions_ds, args.output).cache()
