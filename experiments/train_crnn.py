import os

import json
import numpy as np
import tensorflow as tf

from hawran import dataset, storage
from hawran.charset import syn_v4_charset
from hawran.crnn import CRNN, make_ds, tfds_from_ds
from hawran.tensorflow_utils import CustomMetricsLogger


experiment_name = os.environ['EXPERIMENT_NAME']

gpu_devices = tf.config.list_physical_devices('GPU')
if gpu_devices:
    tf.config.experimental.set_memory_growth(gpu_devices[0], True)

tf.config.threading.set_intra_op_parallelism_threads(13)
tf.config.threading.set_inter_op_parallelism_threads(13)

def stage_schedule(max_size, patience_factor=4):
    size_order = np.floor(np.log2(max_size)).astype(np.int32)
    scale = np.power(2, np.arange(0, size_order + 1)[::-1])

    ex_sizes = max_size // scale
    patiences = patience_factor * scale

    yield from zip(ex_sizes, patiences)


def load_index(path):
    with storage.open(path, 'rt') as index_f:
        return json.load(index_f)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.INFO)

    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('train_fonts')
    parser.add_argument('val_fonts')
    parser.add_argument('--train_shards', nargs='+')
    parser.add_argument('--val_shards', nargs='+')
    parser.add_argument('--loss', choices=('ctc',), default='ctc')
    parser.add_argument('--batch-size', type=int, default=64)
    parser.add_argument('--steps-per-epoch', type=int, default=256)
    parser.add_argument('--enlarging', action='store_true')
    parser.add_argument('--sample-len', type=int, default=64)
    parser.add_argument('--n-epochs', type=int, default=256)
    parser.add_argument('--seed', type=int, default=42)
    parser.add_argument('--weights')
    args = parser.parse_args()

    validation_freq = 4

    train_font_names = load_index(args.train_fonts)
    val_font_names = load_index(args.val_fonts)

    if args.loss == 'ctc':
        CRNNType = CRNN
    else:
        raise NotImplementedError

    crnn = CRNNType(syn_v4_charset)
    crnn.compile(optimizer=tf.keras.optimizers.Adam())

    if args.weights:
        dirname = f's3://hawran/experiments/{args.weights}'
        with storage.open(f'{dirname}/checkpoint', 'rt') as checkpoint_f:
            checkpoint = next(checkpoint_f).split(':')[1].strip(' \n"')
        crnn.load_weights(f's3://hawran/experiments/{args.weights}/{checkpoint}')

    if args.enlarging:
        stages = stage_schedule(args.sample_len)
    else:
        stages = [(args.sample_len, args.n_epochs)]

    last_epoch = 0

    tb_callback = tf.keras.callbacks.TensorBoard(
        f's3://hawran/tensorboard/{experiment_name}',
        profile_batch=0,
    )

    for stage_text_len, patience in stages:
        train_examples_ds = dataset.InterleaveDataset(*(
            make_ds(
                load_index(shards_name),
                train_font_names,
                stage_text_len,
                syn_v4_charset,
                args.batch_size * args.steps_per_epoch,
                args.seed,
            )
            for shards_name in args.train_shards
        ))
        val_examples_ds = dataset.InterleaveDataset(*(
            make_ds(
                load_index(shards_name),
                val_font_names,
                stage_text_len,
                syn_v4_charset,
                args.batch_size * args.steps_per_epoch,
            )
            for shards_name in args.val_shards
        ))

        train_ds = tfds_from_ds(train_examples_ds, args.batch_size)
        val_ds = tfds_from_ds(
            val_examples_ds, args.batch_size, args.steps_per_epoch, True
        )

        history = crnn.fit(
            train_ds,
            initial_epoch=last_epoch,
            epochs=(last_epoch + args.n_epochs),
            steps_per_epoch=args.steps_per_epoch,
            validation_data=val_ds,
            validation_freq=validation_freq,
            callbacks=[
                tf.keras.callbacks.ModelCheckpoint(
                    f's3://hawran/experiments/{experiment_name}/ckpt.{{epoch:03d}}',
                    save_weights_only=True,
                    save_freq=(args.steps_per_epoch * validation_freq),
                ),
                tb_callback,
                tf.keras.callbacks.EarlyStopping(monitor='loss', patience=patience, mode='auto'),
            ],
        )
        last_epoch += len(history.history['loss'])
