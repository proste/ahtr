import json
import re
from collections import defaultdict
from typing import Iterable, List

from tqdm import tqdm

from hawran import dataset, storage
from hawran.charset import Charset, syn_v4_charset
from hawran.language_modelling import SPACE_SUB
from hawran.typesetting import spacify


class TranscriptedDataset(dataset.TransformedDataset):
    def __init__(self, tokens_ds: dataset.Dataset, charset: Charset):
        super().__init__(tokens_ds)
        self._trans_table = defaultdict(lambda: '�')
        self._trans_table.update(
            str.maketrans({k: v for k, (v, _) in charset._trans_table.items()})
        )

    def _transform(self, block_id: str, tokens: List[str]):
        yield {
            'block_id': block_id,
            'tokens': [token.translate(self._trans_table) for token in tokens],
        }


class WordsDataset(dataset.TransformedDataset):
    def __init__(self, tokens_ds: dataset.Dataset, word_delimiters: Iterable[str]):
        super().__init__(tokens_ds)
        self._word_delimiters = frozenset(word_delimiters)

    def _split_token(self, token: str) -> Iterable[str]:
        begin = 0
        for end, char in enumerate(token):
            if char in self._word_delimiters:
                if end > begin:
                    yield token[begin:end]
                yield token[end]
                begin = end + 1
        end = len(token)
        if end > begin:
            yield token[begin:end]

    def _transform(self, block_id: str, tokens: List[str]) -> dict:
        yield {
            'sample_id': block_id,
            'sample': ' '.join(
                token_ for token in tokens for token_ in self._split_token(token)
            ),
        }


class CharsDataset(dataset.TransformedDataset):
    def __init__(self, tokens_ds: dataset.Dataset, space_sub: str = None):
        super().__init__(tokens_ds)
        self._space_sub = space_sub or SPACE_SUB

    def _transform(self, block_id: str, tokens: List[str]):
        text = ' '.join(spacify(tokens).replace(' ', self._space_sub))

        yield {
            'sample_id': block_id,
            'sample': text,
        }


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('shards')
    parser.add_argument('--granularity', default='words', choices=('words', 'chars'))
    args = parser.parse_args()

    with storage.open(args.shards, 'r') as shards_f:
        train_shards = json.load(shards_f)

    shard_names_ds = dataset.ListDataset.from_components(shard_name=train_shards)
    tokens_ds = dataset.SYNv4ShardTokensDataset(shard_names_ds)
    tokens_ds = TranscriptedDataset(tokens_ds, syn_v4_charset)
    if args.granularity == 'chars':
        lm_samples_ds = CharsDataset(tokens_ds)
    else:  # args.granularity == 'words'
        _, word_delimiters = syn_v4_charset.subset(
            syn_v4_charset.ALPHANUM_RE, complement=True
        )
        lm_samples_ds = WordsDataset(tokens_ds, word_delimiters)

    try:
        for sample in tqdm(lm_samples_ds):
            print(sample['sample'])
    except (BrokenPipeError, KeyboardInterrupt):
        pass
