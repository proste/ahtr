import tensorflow as tf

from hawran import dataset
from hawran.charset import syn_v4_charset
from hawran.crnn import CRNN, ExamplesDataset, PreTrainedCRNN, tfds_from_ds

if __name__ == '__main__':
    import argparse
    import os

    experiment_name = os.environ['EXPERIMENT_NAME']

    parser = argparse.ArgumentParser()
    parser.add_argument('weights')
    parser.add_argument('samples')
    parser.add_argument('--mode', choices=('last', 'whole'), default='whole')
    parser.add_argument('--batch-size', type=int, default=8)
    parser.add_argument('--n-epochs', type=int, default=32)
    parser.add_argument('--val-ratio', type=float, default=0.2)
    args = parser.parse_args()

    model_type = {
        'last': PreTrainedCRNN,
        'whole': CRNN,
    }[args.mode]
    crnn = model_type(syn_v4_charset)
    crnn.compile(optimizer=tf.keras.optimizers.Adam(learning_rate=0.0001))
    crnn.load_weights(f's3://hawran/experiments/{args.weights}')

    ds = dataset.NPRecordDataset(args.samples)
    ds = dataset.ShuffleDataset(ds, buffer_len=(args.batch_size * 64), seed=42)
    ds = ExamplesDataset(ds, syn_v4_charset)
    tfds = tfds_from_ds(ds, args.batch_size)

    # TODO create val split
    crnn.fit(tfds, epochs=args.n_epochs)

    crnn.save_weights(f's3://hawran/experiments/{experiment_name}/weights')
