#!/usr/bin/env sh
. .venv/bin/activate

set -e
set -x

python ./experiments/train_crnn.py \
    fonts/210409_train.json \
    fonts/210409_val.json \
    --train_shards texts/210409_train.json texts/giga_210708_train.json \
    --val_shards texts/210409_val.json texts/giga_210708_val.json \
    --batch-size=80 \
    --sample-len=768 \
    --steps-per-epoch=128
